#!/usr/bin/env python

import rosbag
import sys

bag = rosbag.Bag(sys.argv[1])
bag_new = rosbag.Bag(sys.argv[2], 'w')

for topic, msg, t in bag.read_messages(''):
    if 'depth/image_raw' in topic:
      msgnew = msg
      msgnew.is_bigendian = 0
      bag_new.write(topic, msgnew, t)
    else:
      bag_new.write(topic, msg, t)

bag.close()
bag_new.close()