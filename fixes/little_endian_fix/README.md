# Little Endian fix

All bags recorded during and prior to the Naples mission were recorded with a little mistake in the thin drivers. The endianness of the depth images was set to big endian, but it should be small endian. A recent fix in cv_bridge, closing this [issue](https://github.com/ros-perception/vision_opencv/issues/7), now actually made it important how the endianness is set. Based on this we need to change this in all recorded bags. Sadly it wasn't easily possible to fix a bag on the spot, so a copy needs to be made.

### Fixing a single bag
This will copy the bag and fix the endianness without messing with your old data. The old and new bag should not be the same.
```bash
$ ./bag_fix.py <broken_bag> <fixed_bag_destination>
```


### Fixing all bags within a folder.
This will find all bags in the folder and subfolders and fix them all. It will actually overwrite the old bags. Be sure to have at least enough free space to copy the biggest bag.
```bash
$ ./fix_all.sh <folder_with_bags>
```
