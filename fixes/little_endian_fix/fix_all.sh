#!/usr/bin/env sh

input_files=`find $1 -iname '*.bag'`

echo "Do not cancel before it finishes, this might destroy your bag files!"
for x in $input_files; do
  echo "Processing $x"
  ./bag_fix.py $x ${x}_temp
  mv ${x}_temp $x
done
echo "Finished, all bags are fixed :D"