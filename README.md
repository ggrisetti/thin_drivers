Thin Drivers package

A suite of minimal lightweight drivers for mobile platforms and devices running under ROS

Supports:

* Asus Xtion
* Kinect V1
* turtlebot
* activmedia pioneer
* xsens IMU
* MESA Element