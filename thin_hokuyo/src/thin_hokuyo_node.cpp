#include <ros/ros.h>
#include <string.h>
#include <sensor_msgs/LaserScan.h>
#include "hokuyo.h"
#include <iostream>

using namespace std;

static HokuyoLaser urg;
static sensor_msgs::LaserScan scan;
char buf[HOKUYO_BUFSIZE];
int frame_skip;

int main(int argc, char** argv) {
  std::string topic, frame_id, serial_port, model;
  ros::init(argc, argv, "xtion",ros::init_options::AnonymousName);
  ros::NodeHandle n("~");
  
  n.param("topic", topic, std::string("/scan"));
  n.param("frame_id", frame_id, std::string("/laser_frame"));
  n.param("serial_port", serial_port, std::string("/dev/ttyACM0"));
  n.param("model", model, std::string("utm"));
  n.param("frame_skip", frame_skip, 0);



  cerr << "running with params: " << endl;
  cerr << "_serial_port:= " << serial_port << endl;
  cerr << "_frame_id:= " << frame_id << endl;
  cerr << "_topic:= " << topic << endl;
  cerr << "_model:= " << model << endl;
  cerr << "_frame_skip:= " << frame_skip << endl;


  int max_int_range = 0;
  int min_int_range = 0;
  int max_beams = 0;
    
  int type;

  if (model == "urg") {
    max_int_range = URG_MAX_RANGE;
    min_int_range = URG_MIN_RANGE;
    max_beams = URG_MAX_BEAMS;
    scan.angle_increment = URG_ANGULAR_STEP;
    type = 0;
  } else if (model == "utm") {
    max_int_range = UTM_MAX_RANGE;
    min_int_range = UTM_MIN_RANGE;
    max_beams = UTM_MAX_BEAMS;
    scan.angle_increment = UTM_ANGULAR_STEP;
    type = 1;
  } else if (model == "ubg") {
    max_int_range = UBG_MAX_RANGE;
    min_int_range = UBG_MIN_RANGE;
    max_beams = UBG_MAX_BEAMS;
    scan.angle_increment = UBG_ANGULAR_STEP;
    type = 2;
  } else {
    cerr << "unknwn model, aborting" << endl;
    return 0;
  }

  double min_ang_limit, max_ang_limit, min_range, max_range;
  n.param("min_ang_limit", min_ang_limit, -.5*M_PI);
  n.param("max_ang_limit", max_ang_limit, .5*M_PI);
  n.param("min_range", min_range, 1e-3*min_int_range);
  n.param("max_range", max_range, 1e-3*max_int_range);

  if(min_range<1e-3*min_int_range)
    min_range=1e-3*min_int_range;

  if(max_range>1e-3*max_int_range)
    max_range=1e-3*max_int_range;


  // clip angular limits
  if(min_ang_limit<-scan.angle_increment * max_beams/2) 
    min_ang_limit=-scan.angle_increment * max_beams/2;

  if(min_ang_limit>scan.angle_increment * max_beams/2) 
    min_ang_limit=scan.angle_increment * max_beams/2;

  if(max_ang_limit<-scan.angle_increment * max_beams/2)
    max_ang_limit=-scan.angle_increment * max_beams/2;

  if(max_ang_limit>scan.angle_increment * max_beams/2)
    max_ang_limit=scan.angle_increment * max_beams/2;

  // if min_ang_limit> max_ang_limit, flip the angle increment
  if(min_ang_limit>max_ang_limit)
    scan.angle_increment=-scan.angle_increment;
  
  int start_beam_index= round(min_ang_limit/scan.angle_increment)+max_beams/2;

  int num_beams=ceil((max_ang_limit-min_ang_limit)/scan.angle_increment);
  scan.ranges.resize(num_beams);
  scan.angle_min = min_ang_limit;
  scan.angle_max = max_ang_limit;
  scan.range_max = max_range;
  // HACK
  scan.range_min = min_range;
  scan.time_increment = 0;
  scan.scan_time = 0;
  cerr << "_max_beams:= " << max_beams << endl;
  cerr << "_start index:= " << start_beam_index << endl;
  cerr << "_min_range:= " << min_range << endl;
  cerr << "_max_range:= " << max_range << endl;
  cerr << "_min_ang_limit:= " << min_ang_limit << endl;
  cerr << "_max_ang_limit:= " << max_ang_limit << endl;
  cerr << "_n_beams:= " << scan.ranges.size() << endl;
  
  ros::Publisher pub = n.advertise<sensor_msgs::LaserScan>(topic, 10);

  cerr << "Opening " <<  serial_port << " ... " << endl;
  int ntry=5; float tryopen_delay=5.0; int o=0;
  
  while (--ntry>0) {
    o=hokuyo_open_usb(&urg, serial_port.c_str());
    if (o<=0) {
      ROS_ERROR_STREAM("thin_hokuyo_node:: Failure in opening serial port " << serial_port << ". Retry " << ntry);
      ros::Duration(tryopen_delay).sleep();
    }
  }

  if (o<=0) {
      ROS_ERROR_STREAM("thin_hokuyo_node:: Definitive failure in opening serial port " << serial_port);
      return -1;
  }
  

  o=hokuyo_init(&urg,type);
  if (o<=0){
    cerr << "failure in initializing device" << endl;
    return -1;
  }

  o=hokuyo_startContinuous(&urg, 0, urg.maxBeams, 0, 0);
  if (o<=0){
    cerr << "failure in starting continuous mode" << endl;
    return -1;
  }

  cerr << "device started" << endl;

  scan.header.seq = 0;
  scan.header.frame_id = frame_id;


  int skip;
  while (ros::ok()){
    hokuyo_readPacket(&urg, buf, HOKUYO_BUFSIZE,10);
    scan.header.stamp = ros::Time::now();
    HokuyoRangeReading reading;
    hokuyo_parseReading(&reading, buf, 0);
    skip++;
    if (skip<frame_skip)
      continue;
    skip=0;
    for (int i=0; i<scan.ranges.size() && i< reading.n_ranges-start_beam_index; i++){
      scan.ranges[i]=1e-3*reading.ranges[i+start_beam_index];
      if (scan.ranges[i]<min_range || scan.ranges[i]>max_range)
	scan.ranges[i]=scan.range_max;
    }
    pub.publish(scan);
  } // while run
  
  printf("Closing.\n");
  hokuyo_close(&urg);
  return 0;

  
}
