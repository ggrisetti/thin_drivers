#ds modified 2015-06-04
#Thomas Schneider, 2/12/2013
#Pascal Gohl, 21/08/2014
cmake_minimum_required(VERSION 2.8.0)

############
# CATKIN STUFF
############
project(libvisensor_custom)

set(CMAKE_MODULE_PATH ${CMAKE_CURRENT_SOURCE_DIR}/cmake)
find_package(Eigen REQUIRED)

############
# SETTINGS
############
set(PKGNAME visensor_custom)

SET(EXECUTABLE_OUTPUT_PATH ${CMAKE_CURRENT_SOURCE_DIR}/bin)
SET(LIBRARY_OUTPUT_PATH ${CMAKE_CURRENT_SOURCE_DIR}/lib)

###########
# BUILD
###########
FILE(
  GLOB SRCS 
  ${CMAKE_CURRENT_SOURCE_DIR}/src/*.cpp 
  ${CMAKE_CURRENT_SOURCE_DIR}/src/sensors/*.cpp
  ${CMAKE_CURRENT_SOURCE_DIR}/src/networking/*.cpp
  ${CMAKE_CURRENT_SOURCE_DIR}/src/synchronization/*.cpp
  ${CMAKE_CURRENT_SOURCE_DIR}/src/serial_bridge/*.cpp
  ${CMAKE_CURRENT_SOURCE_DIR}/src/visensor/*.cpp
  ${CMAKE_CURRENT_SOURCE_DIR}/src/helpers/*.cpp
)
INCLUDE_DIRECTORIES("include")
include_directories(${EIGEN_INCLUDE_DIR})

#ds release/debug build, disabled warnings
set(CMAKE_CXX_FLAGS "-DVISENSOR_EXPORT -fPIC -fmessage-length=0 -MMD -MP -w -std=c++11 -fvisibility=hidden")
#ADD_DEFINITIONS (-DVISENSOR_EXPORT -fPIC -fmessage-length=0 -MMD -MP -w -std=c++11 -fvisibility=hidden)
if(NOT DEFINED CMAKE_BUILD_TYPE)
    set(CMAKE_BUILD_TYPE Release)
    #ADD_DEFINITIONS (-march=native -O3)
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -march=native -O3")
endif(NOT DEFINED CMAKE_BUILD_TYPE)

#shared library
ADD_LIBRARY(${PKGNAME} SHARED ${SRCS})
SET_TARGET_PROPERTIES(${PKGNAME} PROPERTIES OUTPUT_NAME ${PKGNAME})
TARGET_LINK_LIBRARIES(${PKGNAME} boost_thread boost_system) 

#static library
#ADD_LIBRARY(${PKGNAME}_static STATIC ${SRCS})
#SET_TARGET_PROPERTIES(${PKGNAME}_static PROPERTIES OUTPUT_NAME ${PKGNAME})
#TARGET_LINK_LIBRARIES(${PKGNAME}_static boost_thread boost_system) 

message("libvisensor_custom build configured")

