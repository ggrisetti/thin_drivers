cmake_minimum_required(VERSION 2.8.3)

#ds root directories
set(CMAKE_CURRENT_SOURCE_DIR_ROOT ${CMAKE_CURRENT_SOURCE_DIR})

#ds log environment
message("-------------------------------- cmake: thin_visensor --------------------------------")
message("CMAKE_CURRENT_SOURCE_DIR: ${CMAKE_CURRENT_SOURCE_DIR}")



#ds change path to visensor source location
set(CMAKE_CURRENT_SOURCE_DIR ${CMAKE_CURRENT_SOURCE_DIR}/thirdparty/libvisensor_custom)

#ds parse cmake list of libvisensor (ALWAYS)
include("${CMAKE_CURRENT_SOURCE_DIR}/CMakeLists.txt")



#ds project name
project(thin_visensor)

#ds reset source dir
set(CMAKE_CURRENT_SOURCE_DIR ${CMAKE_CURRENT_SOURCE_DIR_ROOT})

#ds build type
if(NOT DEFINED CMAKE_BUILD_TYPE)

 set(CMAKE_BUILD_TYPE Release)

endif(NOT DEFINED CMAKE_BUILD_TYPE)

#ds find Eigen
find_package(Eigen3 REQUIRED)

#ds find OpenCV
find_package(OpenCV REQUIRED COMPONENTS core highgui imgproc)

#ds ros/catkin
find_package(
catkin REQUIRED COMPONENTS
roscpp 
sensor_msgs 
image_transport 
camera_info_manager 
cmake_modules
cv_bridge
)

catkin_package(
INCLUDE_DIRS ${catkin_INCLUDE_DIRS}
CATKIN_DEPENDS
roscpp 
sensor_msgs 
image_transport 
camera_info_manager 
cv_bridge
)

#ds flags (disabled warnings for now as thirdparty would spam the console)
set(CMAKE_CXX_FLAGS ${EIGEN_DEFINITIONS} "-O3 -march=native -w -std=c++11")

#ds includes
include_directories(${catkin_INCLUDE_DIRS} ${EIGEN_INCLUDE_DIR} "${CMAKE_CURRENT_SOURCE_DIR}/thirdparty/libvisensor_custom/include")

#ds compilation
add_executable(thin_visensor_node src/thin_visensor_node.cpp src/CVISensorPublisher.cpp)

#ds linkage
target_link_libraries(thin_visensor_node ${catkin_LIBRARIES} ${OpenCV_LIBRARIES} visensor_custom)

message("--------------------------------------------------------------------------------------")

