#ifndef CVISENSORPUBLISHER_H
#define CVISENSORPUBLISHER_H

//ds ros
#include <ros/ros.h>
#include <image_transport/image_transport.h>
#include <geometry_msgs/Pose.h>

//ds libvisensor
#include "visensor/visensor_api.hpp"
#include "visensor/visensor_exceptions.hpp"



//ds sensor callback handler
class CVISensorPublisher
{

//ds ctor/dtor (DO NOT THROW)
public:

    CVISensorPublisher( std::shared_ptr< ros::NodeHandle > p_pNode,
                        const uint32_t& p_uHzRateCamera,
                        const uint32_t& p_uHzRateIMU,
                        const uint32_t& p_uMessageQueueSize,
                        const uint64_t& p_uThresholdDataDelayWarningNanoseconds ): m_pNode( p_pNode ),
                                                                                   m_uHzRateCamera( p_uHzRateCamera ),
                                                                                   m_uHzRateIMU( p_uHzRateIMU ),
                                                                                   m_uMessageQueueSize( p_uMessageQueueSize ),
                                                                                   m_uThresholdDataDelayWarningNanoseconds( p_uThresholdDataDelayWarningNanoseconds )
    {
        //ds nothing to do
    }
    ~CVISensorPublisher( )
    {
        //ds nothing to do
    }

//ds members
private:

    std::shared_ptr< ros::NodeHandle > m_pNode;
    visensor::ViSensorDriver m_cVISensorDriver;
    const uint32_t m_uHzRateCamera;
    const uint32_t m_uHzRateIMU;
    const uint32_t m_uMessageQueueSize;
    const uint64_t m_uThresholdDataDelayWarningNanoseconds;

    //ds id mappings
    const std::map< visensor::SensorId::SensorId, std::string > m_mapCameraNames{ { visensor::SensorId::CAM0, "camera_right" },
                                                                                  { visensor::SensorId::CAM1, "camera_left" } };

    //ds camera poses
    std::map< visensor::SensorId::SensorId, geometry_msgs::Pose > m_mapCameraPoses;

    //ds cameras (2)
    std::map< visensor::SensorId::SensorId, image_transport::CameraPublisher > m_mapPublishersImage;
    std::map< visensor::SensorId::SensorId, sensor_msgs::CameraInfo > m_mapPublishersCameraInfo;
    std::map< visensor::SensorId::SensorId, ros::Publisher > m_mapPublishersCameraPose;

    //ds IMU publishers
    ros::Publisher m_cPublisherIMU_ADIS16448;
    ros::Publisher m_cPublisherIMU_MPU9150RIGHT;
    ros::Publisher m_cPublisherIMU_MPU9150LEFT;

    //ds additional publishers
    ros::Publisher m_cPublisherPressure;
    ros::Publisher m_cPublisherTemperatureRIGHT;
    ros::Publisher m_cPublisherTemperatureLEFT;
    ros::Publisher m_cPublisherMagneticField;

//ds accessors
public:

    void connectSensors( );
    void startSensors( );

//ds internal
private:

    const sensor_msgs::CameraInfo _getCameraInfo( const visensor::SensorId::SensorId& p_eCameraID ) const;
    void _setStereoCameraInfo( const visensor::SensorId::SensorId& p_eCameraIDRIGHT,
                               sensor_msgs::CameraInfo& p_msgCameraInfoRIGHT,
                               const visensor::SensorId::SensorId& p_eCameraIDLEFT,
                               sensor_msgs::CameraInfo& p_msgCameraInfoLEFT );
    void _cacheCalibrationMessage( const visensor::SensorId::SensorId& p_eCameraID );
    void _callbackIMU_ADIS16448( const visensor::ViImuMsg::Ptr p_pIMU, visensor::ViErrorCode p_cError );
    void _callbackIMU_MPU9150( const visensor::ViImuMsg::Ptr p_pIMU, visensor::ViErrorCode p_cError );
    void _callbackCamera( const visensor::ViFrame::Ptr p_pCameraFrame, visensor::ViErrorCode p_cError );
};

#endif //CVISENSORPUBLISHER_H
