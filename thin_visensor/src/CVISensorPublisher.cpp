#include "CVISensorPublisher.h"

//ds ros
#include <sensor_msgs/Imu.h>
#include <sensor_msgs/fill_image.h>
#include <sensor_msgs/FluidPressure.h>
#include <sensor_msgs/Temperature.h>
#include <sensor_msgs/MagneticField.h>
#include <tf/transform_broadcaster.h>

#include <Eigen/Dense>
#include <opencv2/calib3d/calib3d.hpp>



void CVISensorPublisher::connectSensors( )
{
    try
    {
        //ds try to initialize the external sensor driver
        m_cVISensorDriver.init( );
    }
    catch( const visensor::exceptions& p_cException )
    {
        std::printf( "<CVISensorPublisher>(connectSensors) could not initialize driver - exception: '%s'\n", p_cException.what( ) );
        throw std::runtime_error( p_cException.what( ) );
    }

    try
    {
        //ds bind callback functions to sensors
        m_cVISensorDriver.setCallbackIMU_ADIS16448( boost::bind( &CVISensorPublisher::_callbackIMU_ADIS16448, this, _1, _2 ) );
        m_cVISensorDriver.setCallbackIMU_MPU9150( boost::bind( &CVISensorPublisher::_callbackIMU_MPU9150, this, _1, _2 ) );
        m_cVISensorDriver.setCameraCallback( boost::bind( &CVISensorPublisher::_callbackCamera, this, _1, _2 ) );
        m_cVISensorDriver.setCameraCalibrationSlot( 0 );
    }
    catch( const visensor::exceptions& p_cException )
    {
        std::printf( "<CVISensorPublisher>(connectSensors) could not register callback functions - exception: '%s'\n", p_cException.what( ) );
        throw std::runtime_error( p_cException.what( ) );
    }

    //ds get camera and imu ids
    const std::vector< visensor::SensorId::SensorId > vecCameraIDs = m_cVISensorDriver.getListOfCameraIDs( );
    const std::vector< visensor::SensorId::SensorId > vecIMUIDs    = m_cVISensorDriver.getListOfImuIDs( );

    //ds log sensor configuration
    for( const visensor::SensorId::SensorId eIDCamera: vecCameraIDs )
    {
        std::printf( "<CVISensorPublisher>(connectSensors) found camera: %i\n", eIDCamera );
    }
    for( const visensor::SensorId::SensorId eIDIMU: vecIMUIDs )
    {
        std::printf( "<CVISensorPublisher>(connectSensors) found IMU: %i\n", eIDIMU );
    }

    //ds check assumed sensor configuration
    if( visensor::SensorId::CAM0 != vecCameraIDs[0] ||
        visensor::SensorId::CAM1 != vecCameraIDs[1] ||
        visensor::SensorId::IMU0 != vecIMUIDs[0]    )
    {
        std::printf( "<CVISensorPublisher>(connectSensors) invalid sensor configuration\n" );
        throw std::runtime_error( "<CVISensorPublisher>(connectSensors) invalid sensor configuration" );
    }

    //ds initialize camera publishers
    for( const visensor::SensorId::SensorId& eCameraID: vecCameraIDs )
    {
        //ds get the camera topic name
        const std::string strCameraName = m_mapCameraNames.at( eCameraID );

        //ds create a fresh camera node
        ros::NodeHandle hCameraNode = ros::NodeHandle( *m_pNode.get( ), strCameraName );

        //ds and image publishers (TODO cosmetic: shrink line)
        m_mapPublishersImage.insert( std::pair< visensor::SensorId::SensorId, image_transport::CameraPublisher >( eCameraID, image_transport::CameraPublisher( ( image_transport::ImageTransport( hCameraNode ) ).advertiseCamera( "image_raw", m_uMessageQueueSize ) ) ) );

        //ds add the camera info publisher
        m_mapPublishersCameraInfo.insert( std::pair< visensor::SensorId::SensorId, sensor_msgs::CameraInfo >( eCameraID, _getCameraInfo( eCameraID ) ) );

        //ds cache the camera calibration data (as calibration SHOULD NOT change during runtime)
        _cacheCalibrationMessage( eCameraID );

        //ds add the camera pose publisher
        m_mapPublishersCameraPose.insert( std::pair< visensor::SensorId::SensorId, ros::Publisher >( eCameraID, m_pNode->advertise< geometry_msgs::Pose >( strCameraName+"/pose_to_imu_adis16448", m_uMessageQueueSize ) ) );
    }

    //ds setup stereo camera configuration
    if( !m_cVISensorDriver.isStereoCameraFlipped( ) )
    {
        //ds generate Stereo ROS configuration, assuming than cam0 and cam1 are in front-parallel stereo configuration
        _setStereoCameraInfo( visensor::SensorId::CAM0, m_mapPublishersCameraInfo.at( visensor::SensorId::CAM0 ), visensor::SensorId::CAM1, m_mapPublishersCameraInfo.at( visensor::SensorId::CAM1 ) );
    }
    else
    {
        //ds generate mirror configuration
        _setStereoCameraInfo( visensor::SensorId::CAM1, m_mapPublishersCameraInfo.at( visensor::SensorId::CAM1 ), visensor::SensorId::CAM0, m_mapPublishersCameraInfo.at( visensor::SensorId::CAM0 ) );
    }

    //ds ALWAYS initialize ADIS16448 publishers
    m_cPublisherIMU_ADIS16448 = m_pNode->advertise< sensor_msgs::Imu >( "imu_adis16448", m_uMessageQueueSize );
    m_cPublisherPressure      = m_pNode->advertise< sensor_msgs::FluidPressure >( "pressure", m_uMessageQueueSize );
    m_cPublisherMagneticField = m_pNode->advertise< sensor_msgs::MagneticField >( "magnetic_field", m_uMessageQueueSize );

    //ds initialize MPU9150 publishers if possible (either one or both)
    if( 2 == vecIMUIDs.size( ) )
    {
        if( visensor::SensorId::IMU_CAM0 == vecIMUIDs[1] )
        {
            std::printf( "<CVISensorPublisher>(connectSensors) created optional MPU9150 publisher on RIGHT camera\n" );
            m_cPublisherIMU_MPU9150RIGHT = m_pNode->advertise< sensor_msgs::Imu >( "camera_right/imu_mpu9150", m_uMessageQueueSize );
            m_cPublisherTemperatureRIGHT = m_pNode->advertise< sensor_msgs::Temperature >( "camera_right/temperature", m_uMessageQueueSize );
        }
        if( visensor::SensorId::IMU_CAM1 == vecIMUIDs[1] )
        {
            std::printf( "<CVISensorPublisher>(connectSensors) created optional MPU9150 publisher on LEFT camera\n" );
            m_cPublisherIMU_MPU9150LEFT = m_pNode->advertise< sensor_msgs::Imu >( "camera_left/imu_mpu9150", m_uMessageQueueSize );
            m_cPublisherTemperatureLEFT = m_pNode->advertise< sensor_msgs::Temperature >( "camera_left/temperature", m_uMessageQueueSize );
        }
    }
    else if( 3 == vecIMUIDs.size( ) )
    {
        std::printf( "<CVISensorPublisher>(connectSensors) created optional MPU9150 publisher on RIGHT camera\n" );
        m_cPublisherIMU_MPU9150RIGHT = m_pNode->advertise< sensor_msgs::Imu >( "camera_right/imu_mpu9150", m_uMessageQueueSize );
        m_cPublisherTemperatureRIGHT = m_pNode->advertise< sensor_msgs::Temperature >( "camera_right/temperature", m_uMessageQueueSize );
        std::printf( "<CVISensorPublisher>(connectSensors) created optional MPU9150 publisher on LEFT camera\n" );
        m_cPublisherIMU_MPU9150LEFT = m_pNode->advertise< sensor_msgs::Imu >( "camera_left/imu_mpu9150", m_uMessageQueueSize );
        m_cPublisherTemperatureLEFT = m_pNode->advertise< sensor_msgs::Temperature >( "camera_left/temperature", m_uMessageQueueSize );
    }
}

void CVISensorPublisher::startSensors( )
{
    m_cVISensorDriver.startAllCameras( m_uHzRateCamera );
    m_cVISensorDriver.startAllImus( m_uHzRateIMU );
}

const sensor_msgs::CameraInfo CVISensorPublisher::_getCameraInfo( const visensor::SensorId::SensorId& p_eCameraID ) const
{
    //ds camera info message to build
    sensor_msgs::CameraInfo msgCameraInfo;

    //ds SNIPPET -------------------------------------------------------------- // TODO
    int image_width = 752;
    int image_height = 480;

    visensor::ViCameraCalibration camera_calibration;
    if (!m_cVISensorDriver.getCameraCalibration(p_eCameraID, camera_calibration))
    {
        std::printf( "<CVISensorPublisher>(_getCameraInfo) failed to get camera info for sensor: %s\n", m_mapCameraNames.at( p_eCameraID ).c_str( ) );
        throw std::runtime_error( "failed to get camera info" );
    }

    double c[9];
    double d[5];

    d[0] = camera_calibration.dist_coeff[0];
    d[1] = camera_calibration.dist_coeff[1];
    d[2] = camera_calibration.dist_coeff[2];
    d[3] = camera_calibration.dist_coeff[3];
    d[4] = 0.0;
    c[0] = camera_calibration.focal_point[0];
    c[1] = 0.0;
    c[2] = camera_calibration.principal_point[0];
    c[3] = 0.0;
    c[4] = camera_calibration.focal_point[1];
    c[5] = camera_calibration.principal_point[1];
    c[6] = 0.0;
    c[7] = 0.0;
    c[8] = 1.0;

    if (msgCameraInfo.D.size() != 5)
        msgCameraInfo.D.resize(5);

    for (int i = 0; i < 5; i++) {
        msgCameraInfo.D[i] = d[i];
    }

    for (int i = 0; i < 9; i++) {
        msgCameraInfo.K[i] = c[i];
    }

    msgCameraInfo.width = image_width;
    msgCameraInfo.height = image_height;

    msgCameraInfo.binning_x = 1;
    msgCameraInfo.binning_y = 1;

    msgCameraInfo.distortion_model = std::string("plumb_bob");
    //ds SNIPPET -------------------------------------------------------------- //

    return msgCameraInfo;
}

void CVISensorPublisher::_setStereoCameraInfo( const visensor::SensorId::SensorId& p_eCameraIDRIGHT,
                                               sensor_msgs::CameraInfo& p_msgCameraInfoRIGHT,
                                               const visensor::SensorId::SensorId& p_eCameraIDLEFT,
                                               sensor_msgs::CameraInfo& p_msgCameraInfoLEFT )
{
    //ds SNIPPET -------------------------------------------------------------- // TODO
    visensor::ViCameraCalibration camera_calibration_0, camera_calibration_1;

    if (!m_cVISensorDriver.getCameraCalibration(p_eCameraIDRIGHT, camera_calibration_0))
    {
        std::printf( "<CVISensorPublisher>(_setStereoCameraInfo) unable to set stereo configuration 1 to 0\n" );
        throw std::runtime_error( "<CVISensorPublisher>(_setStereoCameraInfo) unable to set stereo configuration 1 to 0" );
    }
    if (!m_cVISensorDriver.getCameraCalibration(p_eCameraIDLEFT, camera_calibration_1))
    {
        std::printf( "<CVISensorPublisher>(_setStereoCameraInfo) unable to set stereo configuration 1 to 0\n" );
        throw std::runtime_error( "<CVISensorPublisher>(_setStereoCameraInfo) unable to set stereo configuration 1 to 0" );
    }

    int image_width = 752;
    int image_height = 480;

    double c0[9];
    double d0[5];
    double r0[9];
    double p0[12];
    double rot0[9];
    double t0[3];

    double c1[9];
    double d1[5];
    double r1[9];
    double p1[12];
    double rot1[9];
    double t1[3];

    double r[9];
    double t[3];

    d0[0] = camera_calibration_0.dist_coeff[0];
    d0[1] = camera_calibration_0.dist_coeff[1];
    d0[2] = camera_calibration_0.dist_coeff[2];
    d0[3] = camera_calibration_0.dist_coeff[3];
    d0[4] = 0.0;
    c0[0] = camera_calibration_0.focal_point[0];
    c0[1] = 0.0;
    c0[2] = camera_calibration_0.principal_point[0];
    c0[3] = 0.0;
    c0[4] = camera_calibration_0.focal_point[1];
    c0[5] = camera_calibration_0.principal_point[1];
    c0[6] = 0.0;
    c0[7] = 0.0;
    c0[8] = 1.0;

    d1[0] = camera_calibration_1.dist_coeff[0];
    d1[1] = camera_calibration_1.dist_coeff[1];
    d1[2] = camera_calibration_1.dist_coeff[2];
    d1[3] = camera_calibration_1.dist_coeff[3];
    d1[4] = 0.0;
    c1[0] = camera_calibration_1.focal_point[0];
    c1[1] = 0.0;
    c1[2] = camera_calibration_1.principal_point[0];
    c1[3] = 0.0;
    c1[4] = camera_calibration_1.focal_point[1];
    c1[5] = camera_calibration_1.principal_point[1];
    c1[6] = 0.0;
    c1[7] = 0.0;
    c1[8] = 1.0;

    for (int i = 0; i < 9; ++i) {
    rot0[i] = camera_calibration_0.R[i];
    rot1[i] = camera_calibration_1.R[i];
    }
    for (int i = 0; i < 3; ++i) {
    t0[i] = camera_calibration_0.t[i];
    t1[i] = camera_calibration_1.t[i];
    }

    Eigen::Map < Eigen::Matrix3d > RR0(rot0);
    Eigen::Map < Eigen::Vector3d > tt0(t0);
    Eigen::Map < Eigen::Matrix3d > RR1(rot1);
    Eigen::Map < Eigen::Vector3d > tt1(t1);

    Eigen::Matrix4d T0 = Eigen::Matrix4d::Zero();
    Eigen::Matrix4d T1 = Eigen::Matrix4d::Zero();

    T0.block<3, 3>(0, 0) = RR0;
    T0.block<3, 1>(0, 3) = tt0;
    T0(3, 3) = 1.0;
    T1.block<3, 3>(0, 0) = RR1;
    T1.block<3, 1>(0, 3) = tt1;
    T1(3, 3) = 1.0;

    Eigen::Matrix4d T_rel = Eigen::Matrix4d::Zero();
    T_rel = T1 * T0.inverse();

    Eigen::Map < Eigen::Matrix3d > R_rel(r);
    Eigen::Map < Eigen::Vector3d > t_rel(t);

    R_rel = T_rel.block<3, 3>(0, 0);
    t_rel << T_rel(0, 3), T_rel(1, 3), T_rel(2, 3);

    double r_temp[9];
    r_temp[0] = R_rel(0, 0);
    r_temp[1] = R_rel(0, 1);
    r_temp[2] = R_rel(0, 2);
    r_temp[3] = R_rel(1, 0);
    r_temp[4] = R_rel(1, 1);
    r_temp[5] = R_rel(1, 2);
    r_temp[6] = R_rel(2, 0);
    r_temp[7] = R_rel(2, 1);
    r_temp[8] = R_rel(2, 2);

    //cv::Mat wrapped(rows, cols, CV_32FC1, external_mem, CV_AUTOSTEP);
    cv::Mat C0(3, 3, CV_64FC1, c0, 3 * sizeof(double));
    cv::Mat D0(5, 1, CV_64FC1, d0, 1 * sizeof(double));
    cv::Mat R0(3, 3, CV_64FC1, r0, 3 * sizeof(double));
    cv::Mat P0(3, 4, CV_64FC1, p0, 4 * sizeof(double));

    cv::Mat C1(3, 3, CV_64FC1, c1, 3 * sizeof(double));
    cv::Mat D1(5, 1, CV_64FC1, d1, 1 * sizeof(double));
    cv::Mat R1(3, 3, CV_64FC1, r1, 3 * sizeof(double));
    cv::Mat P1(3, 4, CV_64FC1, p1, 4 * sizeof(double));

    cv::Mat R(3, 3, CV_64FC1, r_temp, 3 * sizeof(double));

    cv::Mat T(3, 1, CV_64FC1, t, 1 * sizeof(double));

    cv::Size img_size(image_width, image_height);

    cv::Rect roi1, roi2;
    cv::Mat Q;

    cv::stereoRectify(C0, D0, C1, D1, img_size, R, T, R0, R1, P0, P1, Q, cv::CALIB_ZERO_DISPARITY, 0,
                    img_size, &roi1, &roi2);

    if (p_msgCameraInfoRIGHT.D.size() != 5)
    p_msgCameraInfoRIGHT.D.resize(5);

    if (p_msgCameraInfoLEFT.D.size() != 5)
    p_msgCameraInfoLEFT.D.resize(5);

    for (int i = 0; i < 5; i++) {
    p_msgCameraInfoRIGHT.D[i] = d0[i];
    p_msgCameraInfoLEFT.D[i] = d1[i];
    }
    for (int i = 0; i < 9; i++) {
    p_msgCameraInfoRIGHT.K[i] = c0[i];
    p_msgCameraInfoRIGHT.R[i] = R0.at<double>(i);
    p_msgCameraInfoLEFT.K[i] = c1[i];
    p_msgCameraInfoLEFT.R[i] = R1.at<double>(i);
    }
    for (int i = 0; i < 12; i++) {
    p_msgCameraInfoRIGHT.P[i] = P0.at<double>(i);
    p_msgCameraInfoLEFT.P[i] = P1.at<double>(i);
    }
    p_msgCameraInfoRIGHT.width = image_width;
    p_msgCameraInfoLEFT.width = image_width;

    p_msgCameraInfoRIGHT.height = image_height;
    p_msgCameraInfoLEFT.height = image_height;

    p_msgCameraInfoRIGHT.binning_x = 1;
    p_msgCameraInfoRIGHT.binning_y = 1;
    p_msgCameraInfoLEFT.binning_x = 1;
    p_msgCameraInfoLEFT.binning_y = 1;

    p_msgCameraInfoRIGHT.distortion_model = std::string("plumb_bob");
    p_msgCameraInfoLEFT.distortion_model = std::string("plumb_bob");
    //ds SNIPPET -------------------------------------------------------------- //
}

void CVISensorPublisher::_cacheCalibrationMessage( const visensor::SensorId::SensorId& p_eCameraID )
{
    //ds visensor calibration
    visensor::ViCameraCalibration cCameraCalibration;

    if( !m_cVISensorDriver.getCameraCalibration( p_eCameraID, cCameraCalibration ) )
    {
        std::printf( "<CVISensorPublisher>(_cacheCalibrationMessage) unable to retrieve calibration information\n" );
        throw std::runtime_error( "<CVISensorPublisher>(_cacheCalibrationMessage)" );
    }

    //ds derive the pose
    tf::Matrix3x3 R_IC( cCameraCalibration.R[0], cCameraCalibration.R[3], cCameraCalibration.R[6] ,
                        cCameraCalibration.R[1], cCameraCalibration.R[4], cCameraCalibration.R[7] ,
                        cCameraCalibration.R[2], cCameraCalibration.R[5], cCameraCalibration.R[8] );

    //ds derive quaternion
    tf::Quaternion q_IC;
    R_IC.getRotation( q_IC );

    //ds get an empty pose message
    geometry_msgs::Pose msgPose;

    //ds set the pose message
    msgPose.orientation.x = q_IC.x( );
    msgPose.orientation.y = q_IC.y( );
    msgPose.orientation.z = q_IC.z( );
    msgPose.orientation.w = q_IC.w( );
    msgPose.position.x    = cCameraCalibration.t[0];
    msgPose.position.y    = cCameraCalibration.t[1];
    msgPose.position.z    = cCameraCalibration.t[2];

    //ds cache the message
    m_mapCameraPoses.insert( std::pair< visensor::SensorId::SensorId, geometry_msgs::Pose >( p_eCameraID, msgPose ) );
}

void CVISensorPublisher::_callbackIMU_ADIS16448( const visensor::ViImuMsg::Ptr p_pIMU, visensor::ViErrorCode p_cError )
{
    //ds check for an invalid measurement
    if( visensor::ViErrorCodes::MEASUREMENT_DROPPED == p_cError )
    {
        //ds log and skip processing
        std::printf( "<CVISensorPublisher>(_callbackIMU_ADIS16448) dropped IMU measurement on sensor: imu_adis16448 (check network bandwidth/sensor rate)\n" );
        return;
    }

    //ds get sensor time of message
    ros::Time tmTimestamp;
    tmTimestamp.fromNSec( p_pIMU->timestamp );

    //ds build the IMU message
    sensor_msgs::Imu msgIMU;
    msgIMU.header.stamp    = tmTimestamp;
    msgIMU.header.frame_id = "imu_adis16448";

    //ds COMPASS: orientation
    msgIMU.orientation.x   = 0.0;
    msgIMU.orientation.y   = 0.0;
    msgIMU.orientation.z   = 0.0;
    msgIMU.orientation.w   = 1.0;
    msgIMU.orientation_covariance[0] = 99999.9;
    msgIMU.orientation_covariance[1] = 0.0;
    msgIMU.orientation_covariance[2] = 0.0;
    msgIMU.orientation_covariance[3] = 0.0;
    msgIMU.orientation_covariance[4] = 99999.9;
    msgIMU.orientation_covariance[5] = 0.0;
    msgIMU.orientation_covariance[6] = 0.0;
    msgIMU.orientation_covariance[7] = 0.0;
    msgIMU.orientation_covariance[8] = 99999.9;

    //ds GYROSCOPE: angular Velocity
    msgIMU.angular_velocity.x = p_pIMU->gyro[0];
    msgIMU.angular_velocity.y = p_pIMU->gyro[1];
    msgIMU.angular_velocity.z = p_pIMU->gyro[2];
    msgIMU.angular_velocity_covariance[0] = 0.0;
    msgIMU.angular_velocity_covariance[1] = 0.0;
    msgIMU.angular_velocity_covariance[2] = 0.0;
    msgIMU.angular_velocity_covariance[3] = 0.0;
    msgIMU.angular_velocity_covariance[4] = 0.0;
    msgIMU.angular_velocity_covariance[5] = 0.0;
    msgIMU.angular_velocity_covariance[6] = 0.0;
    msgIMU.angular_velocity_covariance[7] = 0.0;
    msgIMU.angular_velocity_covariance[8] = 0.0;

    //ds ACCELEROMETER: linear Acceleration
    msgIMU.linear_acceleration.x = p_pIMU->acc[0];
    msgIMU.linear_acceleration.y = p_pIMU->acc[1];
    msgIMU.linear_acceleration.z = p_pIMU->acc[2];
    msgIMU.linear_acceleration_covariance[0] = 0.0;
    msgIMU.linear_acceleration_covariance[1] = 0.0;
    msgIMU.linear_acceleration_covariance[2] = 0.0;
    msgIMU.linear_acceleration_covariance[3] = 0.0;
    msgIMU.linear_acceleration_covariance[4] = 0.0;
    msgIMU.linear_acceleration_covariance[5] = 0.0;
    msgIMU.linear_acceleration_covariance[6] = 0.0;
    msgIMU.linear_acceleration_covariance[7] = 0.0;
    msgIMU.linear_acceleration_covariance[8] = 0.0;

    //ds build the pressure message
    sensor_msgs::FluidPressure msgPressure;
    msgPressure.header.stamp    = tmTimestamp;
    msgPressure.header.frame_id = "pressure";
    msgPressure.fluid_pressure  = p_pIMU->baro;
    msgPressure.variance        = 0.0;

    //ds build the magnetic field message
    sensor_msgs::MagneticField msgMagneticField;
    msgMagneticField.header.stamp                 = tmTimestamp;
    msgMagneticField.header.frame_id              = "magnetic_field";
    msgMagneticField.magnetic_field.x             = p_pIMU->mag[0];
    msgMagneticField.magnetic_field.y             = p_pIMU->mag[1];
    msgMagneticField.magnetic_field.z             = p_pIMU->mag[2];
    msgMagneticField.magnetic_field_covariance[0] = 0.0;
    msgMagneticField.magnetic_field_covariance[1] = 0.0;
    msgMagneticField.magnetic_field_covariance[2] = 0.0;
    msgMagneticField.magnetic_field_covariance[3] = 0.0;
    msgMagneticField.magnetic_field_covariance[4] = 0.0;
    msgMagneticField.magnetic_field_covariance[5] = 0.0;
    msgMagneticField.magnetic_field_covariance[6] = 0.0;
    msgMagneticField.magnetic_field_covariance[7] = 0.0;
    msgMagneticField.magnetic_field_covariance[8] = 0.0;

    //ds publish all messages
    m_cPublisherIMU_ADIS16448.publish( msgIMU );
    m_cPublisherPressure.publish( msgPressure );
    m_cPublisherMagneticField.publish( msgMagneticField );
}

void CVISensorPublisher::_callbackIMU_MPU9150( const visensor::ViImuMsg::Ptr p_pIMU, visensor::ViErrorCode p_cError )
{
    //ds check for an invalid measurement
    if( visensor::ViErrorCodes::MEASUREMENT_DROPPED == p_cError )
    {
        //ds log and skip processing
        std::printf( "<CVISensorPublisher>(_callbackIMU_MPU9150) dropped IMU measurement on sensor: imu_mpu9150 (check network bandwidth/sensor rate)\n" );
        return;
    }

    //ds get sensor time of message
    ros::Time tmTimestamp;
    tmTimestamp.fromNSec( p_pIMU->timestamp );

    //ds build the IMU message
    sensor_msgs::Imu msgIMU;
    msgIMU.header.stamp = tmTimestamp;

    //ds check if RIGHT camera
    if( visensor::SensorId::IMU_CAM0 == static_cast< visensor::SensorId::SensorId>( p_pIMU->imu_id ) )
    {
        msgIMU.header.frame_id = "camera_right/imu_mpu9150";

        //ds COMPASS: orientation
        msgIMU.orientation.x   = 0.0;
        msgIMU.orientation.y   = 0.0;
        msgIMU.orientation.z   = 0.0;
        msgIMU.orientation.w   = 1.0;
        msgIMU.orientation_covariance[0] = 99999.9;
        msgIMU.orientation_covariance[1] = 0.0;
        msgIMU.orientation_covariance[2] = 0.0;
        msgIMU.orientation_covariance[3] = 0.0;
        msgIMU.orientation_covariance[4] = 99999.9;
        msgIMU.orientation_covariance[5] = 0.0;
        msgIMU.orientation_covariance[6] = 0.0;
        msgIMU.orientation_covariance[7] = 0.0;
        msgIMU.orientation_covariance[8] = 99999.9;

        //ds GYROSCOPE: angular Velocity
        msgIMU.angular_velocity.x = p_pIMU->gyro[0];
        msgIMU.angular_velocity.y = p_pIMU->gyro[1];
        msgIMU.angular_velocity.z = p_pIMU->gyro[2];
        msgIMU.angular_velocity_covariance[0] = 0.0;
        msgIMU.angular_velocity_covariance[1] = 0.0;
        msgIMU.angular_velocity_covariance[2] = 0.0;
        msgIMU.angular_velocity_covariance[3] = 0.0;
        msgIMU.angular_velocity_covariance[4] = 0.0;
        msgIMU.angular_velocity_covariance[5] = 0.0;
        msgIMU.angular_velocity_covariance[6] = 0.0;
        msgIMU.angular_velocity_covariance[7] = 0.0;
        msgIMU.angular_velocity_covariance[8] = 0.0;

        //ds ACCELEROMETER: linear Acceleration
        msgIMU.linear_acceleration.x = p_pIMU->acc[0];
        msgIMU.linear_acceleration.y = p_pIMU->acc[1];
        msgIMU.linear_acceleration.z = p_pIMU->acc[2];
        msgIMU.linear_acceleration_covariance[0] = 0.0;
        msgIMU.linear_acceleration_covariance[1] = 0.0;
        msgIMU.linear_acceleration_covariance[2] = 0.0;
        msgIMU.linear_acceleration_covariance[3] = 0.0;
        msgIMU.linear_acceleration_covariance[4] = 0.0;
        msgIMU.linear_acceleration_covariance[5] = 0.0;
        msgIMU.linear_acceleration_covariance[6] = 0.0;
        msgIMU.linear_acceleration_covariance[7] = 0.0;
        msgIMU.linear_acceleration_covariance[8] = 0.0;

        //ds build the temperature message
        sensor_msgs::Temperature msgTemperature;
        msgTemperature.header.stamp    = tmTimestamp;
        msgTemperature.header.frame_id = "camera_right/temperature";
        msgTemperature.temperature     = p_pIMU->temperature;
        msgTemperature.variance        = 0.0;

        //ds publish the messages
        m_cPublisherIMU_MPU9150RIGHT.publish( msgIMU );
        m_cPublisherTemperatureRIGHT.publish( msgTemperature );
    }
    else
    {
        msgIMU.header.frame_id = "camera_left/imu_mpu9150";

        //ds COMPASS: orientation
        msgIMU.orientation.x   = 0.0;
        msgIMU.orientation.y   = 0.0;
        msgIMU.orientation.z   = 0.0;
        msgIMU.orientation.w   = 1.0;
        msgIMU.orientation_covariance[0] = 99999.9;
        msgIMU.orientation_covariance[1] = 0.0;
        msgIMU.orientation_covariance[2] = 0.0;
        msgIMU.orientation_covariance[3] = 0.0;
        msgIMU.orientation_covariance[4] = 99999.9;
        msgIMU.orientation_covariance[5] = 0.0;
        msgIMU.orientation_covariance[6] = 0.0;
        msgIMU.orientation_covariance[7] = 0.0;
        msgIMU.orientation_covariance[8] = 99999.9;

        //ds GYROSCOPE: angular Velocity
        msgIMU.angular_velocity.x = p_pIMU->gyro[0];
        msgIMU.angular_velocity.y = p_pIMU->gyro[1];
        msgIMU.angular_velocity.z = p_pIMU->gyro[2];
        msgIMU.angular_velocity_covariance[0] = 0.0;
        msgIMU.angular_velocity_covariance[1] = 0.0;
        msgIMU.angular_velocity_covariance[2] = 0.0;
        msgIMU.angular_velocity_covariance[3] = 0.0;
        msgIMU.angular_velocity_covariance[4] = 0.0;
        msgIMU.angular_velocity_covariance[5] = 0.0;
        msgIMU.angular_velocity_covariance[6] = 0.0;
        msgIMU.angular_velocity_covariance[7] = 0.0;
        msgIMU.angular_velocity_covariance[8] = 0.0;

        //ds ACCELEROMETER: linear Acceleration
        msgIMU.linear_acceleration.x = p_pIMU->acc[0];
        msgIMU.linear_acceleration.y = p_pIMU->acc[1];
        msgIMU.linear_acceleration.z = p_pIMU->acc[2];
        msgIMU.linear_acceleration_covariance[0] = 0.0;
        msgIMU.linear_acceleration_covariance[1] = 0.0;
        msgIMU.linear_acceleration_covariance[2] = 0.0;
        msgIMU.linear_acceleration_covariance[3] = 0.0;
        msgIMU.linear_acceleration_covariance[4] = 0.0;
        msgIMU.linear_acceleration_covariance[5] = 0.0;
        msgIMU.linear_acceleration_covariance[6] = 0.0;
        msgIMU.linear_acceleration_covariance[7] = 0.0;
        msgIMU.linear_acceleration_covariance[8] = 0.0;

        //ds build the temperature message
        sensor_msgs::Temperature msgTemperature;
        msgTemperature.header.stamp    = tmTimestamp;
        msgTemperature.header.frame_id = "camera_left/temperature";
        msgTemperature.temperature     = p_pIMU->temperature;
        msgTemperature.variance        = 0.0;

        //ds publish the messages
        m_cPublisherIMU_MPU9150LEFT.publish( msgIMU );
        m_cPublisherTemperatureLEFT.publish( msgTemperature );
    }
}

void CVISensorPublisher::_callbackCamera( const visensor::ViFrame::Ptr p_pCameraFrame, visensor::ViErrorCode p_cError )
{
    //ds get the camera id
    const visensor::SensorId::SensorId eCameraID = static_cast< visensor::SensorId::SensorId>( p_pCameraFrame->camera_id );

    //ds check for an invalid measurement
    if( visensor::ViErrorCodes::MEASUREMENT_DROPPED == p_cError )
    {
        //ds log and skip processing
        std::printf( "<CVISensorPublisher>(_callbackCamera) dropped camera image on sensor: %s (check network bandwidth/sensor rate)\n", m_mapCameraNames.at( eCameraID ).c_str( ) );
        return;
    }

    //ds get sensor time of message
    ros::Time tmTimestamp;
    tmTimestamp.fromNSec( p_pCameraFrame->timestamp );

    //ds get image dimensions
    const uint32_t uImageHeight = p_pCameraFrame->height;
    const uint32_t uImageWidth  = p_pCameraFrame->width;

    //ds build sensor message
    sensor_msgs::Image msgImage;
    msgImage.header.stamp    = tmTimestamp;
    msgImage.header.frame_id = m_mapCameraNames.at( eCameraID );

    //ds fill the image data (assuming p_pCameraFrame->image_type == visensor::MONO8)
    sensor_msgs::fillImage( msgImage, sensor_msgs::image_encodings::MONO8, uImageHeight, uImageWidth, uImageWidth, p_pCameraFrame->getImageRawPtr( ) );

    //ds get current CameraInfo data
    sensor_msgs::CameraInfo msgCameraInfo = m_mapPublishersCameraInfo[eCameraID];

    //ds build camera info message
    msgCameraInfo.header.frame_id = msgImage.header.frame_id;
    msgCameraInfo.header.stamp    = tmTimestamp;
    msgCameraInfo.height          = uImageHeight;
    msgCameraInfo.width           = uImageWidth;

    //ds publish the image with the camera info
    m_mapPublishersImage[eCameraID].publish( msgImage, msgCameraInfo );
    m_mapPublishersCameraPose[eCameraID].publish( m_mapCameraPoses[eCameraID] );
}
