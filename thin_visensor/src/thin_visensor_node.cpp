#include "CVISensorPublisher.h"

//ds command line parsing (setting params IN/OUT)
void setParametersNaive( const int& p_iArgc,
                         char** const p_pArgv,
                         uint32_t& p_uHzRateCamera,
                         uint32_t& p_uHzRateIMU,
                         uint32_t& p_uMessageQueueSize,
                         uint64_t& p_uThresholdDataDelayWarningNanoseconds );

int main( int argc, char** argv )
{
    std::fflush( stdout );
    std::printf( "(main) launched: %s\n", argv[0] );

    //ds default parameters
    uint32_t uHzRateCamera                        ( 20 );
    uint32_t uHzRateIMU                           ( 200 );
    uint32_t uMessageQueueSize                    ( 1000 );
    uint64_t uThresholdDataDelayWarningNanoseconds( 100000000 );

    //ds parse command line arguments
    setParametersNaive( argc, argv, uHzRateCamera, uHzRateIMU, uMessageQueueSize, uThresholdDataDelayWarningNanoseconds );

    //ds set up node
    const std::string strNodeNamespace( "thin_visensor_node" );
    ros::init( argc, argv, strNodeNamespace );
    std::shared_ptr< ros::NodeHandle > pNode( new ros::NodeHandle( "~" ) );

    //ds log configuration
    std::printf( "(main) ----------------------------------------------------- CONFIGURATION ----------------------------------------------------- \n" );
    std::printf( "(main) ROS node namespace                    := '%s'\n", pNode->getNamespace( ).c_str( ) );
    std::printf( "(main) uHzRateCamera                         := '%u'\n", uHzRateCamera );
    std::printf( "(main) uHzRateIMU                            := '%u'\n", uHzRateIMU );
    std::printf( "(main) uMessageQueueSize                     := '%u'\n", uMessageQueueSize );
    std::printf( "(main) uThresholdDataDelayWarningNanoseconds := '%lu'\n", uThresholdDataDelayWarningNanoseconds );
    std::printf( "(main) ------------------------------------------------------------------------------------------------------------------------- \n" );
    std::fflush( stdout );

    //ds get a callback handler for the sensors
    CVISensorPublisher cSensorPublisher = CVISensorPublisher( pNode, uHzRateCamera, uHzRateIMU, uMessageQueueSize, uThresholdDataDelayWarningNanoseconds );

    try
    {
        //ds try to connect the sensors with the driver
        cSensorPublisher.connectSensors( );

        //ds start sensors
        cSensorPublisher.startSensors( );
    }
    catch( const std::runtime_error& p_cException )
    {
        std::printf( "(main) failed to start sensors: '%s' - aborting\n", p_cException.what( ) );
        std::printf( "(main) terminated: %s\n", argv[0] );
        std::fflush( stdout );
        return -1;
    }

    //ds enable callback pump
    ros::spin( );

    std::printf( "\n(main) terminated: %s\n", argv[0] );
    std::fflush( stdout );
    return 0;
}

void setParametersNaive( const int& p_iArgc,
                         char** const p_pArgv,
                         uint32_t& p_uHzRateCamera,
                         uint32_t& p_uHzRateIMU,
                         uint32_t& p_uMessageQueueSize,
                         uint64_t& p_uThresholdDataDelayWarningNanoseconds )
{
    //ds attribute names (C style for printf)
    const char* arrParameter1( "-f_camera_hz" );
    const char* arrParameter2( "-f_imu_hz" );
    const char* arrParameter3( "-queue" );
    const char* arrParameter4( "-delay_ms" );

    //ds even number of parameters indicates invalid call (except for help)
    if( 2 < p_iArgc && 0 == p_iArgc%2 )
    {
        std::printf( "(setParametersNaive) malformed command line syntax, usage: thin_visensor_node %s 20 %s 200 %s 10 %s 100000000\n", arrParameter1, arrParameter2, arrParameter3, arrParameter4 );
        std::printf( "(setParametersNaive) terminated: %s\n", p_pArgv[0] );
        std::fflush( stdout );
        exit( -1 );
    }

    //ds check for help call (doesn't matter which second argument is used)
    if( 2 == p_iArgc )
    {
        std::printf( "(setParametersNaive) usage: thin_visensor_node %s 20 %s 200 %s 10 %s 100000000\n", arrParameter1, arrParameter2, arrParameter3, arrParameter4 );
        std::printf( "(setParametersNaive) terminated: %s\n", p_pArgv[0] );
        std::fflush( stdout );
        exit( 0 );
    }

    //ds parse optional command line arguments: -f_camera_hz 20 -f_imu_hz 200 -queue 10 -delay_ms 100000000
    std::vector< std::string > vecCommandLineArguments( p_iArgc-1 );
    for( uint32_t u = 1; u < static_cast< uint32_t >( p_iArgc ); ++u )
    {
        vecCommandLineArguments[u-1] = p_pArgv[u];
    }

    //ds check possible parameters
    const std::vector< std::string >::const_iterator itParameter1( std::find( vecCommandLineArguments.begin( ), vecCommandLineArguments.end( ), arrParameter1 ) );
    const std::vector< std::string >::const_iterator itParameter2( std::find( vecCommandLineArguments.begin( ), vecCommandLineArguments.end( ), arrParameter2 ) );
    const std::vector< std::string >::const_iterator itParameter3( std::find( vecCommandLineArguments.begin( ), vecCommandLineArguments.end( ), arrParameter3 ) );
    const std::vector< std::string >::const_iterator itParameter4( std::find( vecCommandLineArguments.begin( ), vecCommandLineArguments.end( ), arrParameter4 ) );

    try
    {
        //ds set parameters if found
        if( vecCommandLineArguments.end( ) != itParameter1 ){ p_uHzRateCamera                         = std::stoi( *( itParameter1+1 ) ); }
        if( vecCommandLineArguments.end( ) != itParameter2 ){ p_uHzRateIMU                            = std::stoi( *( itParameter2+1 ) ); }
        if( vecCommandLineArguments.end( ) != itParameter3 ){ p_uMessageQueueSize                     = std::stoi( *( itParameter3+1 ) ); }
        if( vecCommandLineArguments.end( ) != itParameter4 ){ p_uThresholdDataDelayWarningNanoseconds = std::stoll( *( itParameter4+1 ) ); }
    }
    catch( const std::invalid_argument& p_cException )
    {
        std::printf( "(setParametersNaive) malformed command line syntax, usage: thin_visensor_node %s 20 %s 200 %s 10 %s 100000000\n", arrParameter1, arrParameter2, arrParameter3, arrParameter4 );
        std::printf( "(setParametersNaive) terminated: %s\n", p_pArgv[0] );
        std::fflush( stdout );
        exit( -1 );
    }
    catch( const std::out_of_range& p_cException )
    {
        std::printf( "(setParametersNaive) malformed command line syntax, usage: thin_visensor_node %s 20 %s 200 %s 10 %s 100000000\n", arrParameter1, arrParameter2, arrParameter3, arrParameter4 );
        std::printf( "(setParametersNaive) terminated: %s\n", p_pArgv[0] );
        std::fflush( stdout );
        exit( -1 );
    }
}
