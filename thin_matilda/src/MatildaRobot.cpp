#include <unistd.h>
#include <cstdio>
#include <cmath>
#include <cstdlib>
#include <sstream>
#include <cstring>
#include <iostream>
#include "serial.h"
#include "MatildaRobot.h"
using namespace std;

/*
OLD PROTOCOL:
l=1:          read odometry               -> PLxxx$                                   xxx = odometry
ll=yyy l=2:   send velocity commands      -> PLxxx $ yyy                              xxx = odometry, yyy = velocity 
l=3:          extended info               -> BLvv # ii # jj # cc # dd # vvv # vv $    vv = velocity, ii = current current, jj = current voltage, 
                                                                                      dd = total coulombs used, vvv = current averaged voltage, vv = counter for voltage average
                                                                                      vvv 55000 is maximum (full charge), 44000 is minimum (fully discharged)
ll=xxx l=4:   force total capacity        -> KL
ll=xxx l=5:   set current used coulombs   -> KL

NEW PROTOCOL
l=1             version info
b=11            save odometry value (both motors)   -> (no response)
l=11            retrieve odometry/speed value       -> R11L1,xxx,sss$                        xxx = odometry ticks, sss = motor speed
l=110           retrieve odometry/speed value+      -> R11L1,xxx,sss,vvv$                    xxx = odometry ticks, sss = motor speed, vvv = motor desired speed
b=33            save extended info (both motors)    -> (no response)
l=33            retrieve extended info              -> R33L1,xxx,vv,ii,jj,cc,dd,vvv$         xxx = odometry ticks, vv = current velocity, ii = current current, jj = current voltage,
                                                                                            dd = total coulombs used, www = current averaged voltage
                                                                                            www 55000 is maximum (full charge), www 44000 is minimum (fully discharged)
ll=left_speed rr=right_speed b=22
                set motor speed                     -> (no response)
*/

MatildaRobot::MatildaRobot() : 
    _fd(0), _x(0.), _y(0.), _theta(0.), _tv(0.), _rv(0.), 
    _des_tv(0.), _des_rv(0.), _new_des_v(false),
    _left_battery(0.), _right_battery(0.),
    _param_ticks_l(0.825e-6), _param_ticks_r(0.825e-6), _param_baseline(0.53), _param_baseline_half(_param_baseline/2), 
    _param_time_interval(65536./(9900.0*8000.0)),
    _odo_last_l(0), _odo_last_r(0), _odo_val_l(0), _odo_val_r(0), 
    _ticks_left(0), _ticks_right(0),
    _protocol(0), _odom_extended_info_ratio(10), _odom_extended_info_counter(0),
    _logging(false), _des_lin(0), _des_ang(0) { }

MatildaRobot::~MatildaRobot() { }

void MatildaRobot::connect(const string& device) {
    _fd = serial_open(device.c_str());
    serial_set_interface_attribs(_fd, B38400 /*B9600*/, 0);
    string lmsg, rmsg;
    for (size_t i = 0; i < 10; i++) {
        send("l=1", 1);
        lmsg = recv();
        if (lmsg != "") break;
    }
    for (size_t i = 0; i < 10; i++) {
        send("r=1", 2);
        rmsg = recv();
        if (rmsg != "") break;
    }
    if (lmsg == "" || rmsg == "") {
        cerr << "Cannot communicate with motors: left [" << lmsg << "], right [" << rmsg << "]" << endl;
        exit(0);
    }
    cout << "Connection successful with motors: left [" << lmsg << "], right [" << rmsg << "]" << endl;
    if (lmsg.find("V7") == string::npos || rmsg.find("V7") == string::npos) {
        cerr << "ERROR: the version of the ROS node does not match the version of the motor programs (expected version: V7)" << endl;
        exit(-1);
    }
}

void MatildaRobot::disconnect() {
}

void MatildaRobot::setSpeed(double tv, double rv) {
    _des_tv = tv; _des_rv = rv;
    _new_des_v = true;
}

void MatildaRobot::getOdometry(double& x, double& y, double& theta) {
    x = _x; y = _y; theta = _theta;
}

void MatildaRobot::getSpeed(double& tv, double& rv) {
    tv = _tv; rv = _rv;
}

void MatildaRobot::send(const string& cmd, int channel) {
    char buf[2];
    buf[0] = channel + 128;
    buf[1] = ' ';
    write(_fd, buf, 2);
    //cout << "SEND: '" << cmd << "'; ";
    write(_fd, (" " + cmd + " ").c_str(), cmd.size() + 2);
}

string MatildaRobot::recv() {
    char buffer[128];
    char* curs = buffer;
    while (buffer == curs || buffer[curs - buffer - 1] != '\n') {
        fd_set read_fds, write_fds, except_fds;
        FD_ZERO(&read_fds); FD_ZERO(&write_fds); FD_ZERO(&except_fds);
        FD_SET(_fd, &read_fds);
        struct timeval timeout;
        timeout.tv_sec = 0;
        timeout.tv_usec = 0.01 * 1e6;
        if (select(_fd + 1, &read_fds, &write_fds, &except_fds, &timeout) == 1) {
            int r = read(_fd, curs, 128 - (curs - buffer));
            if (r == 0) {
                //cout << "READ0" << endl;
                return "";  // disconnected?
            }
            curs += r;
        }
        else {
            //cout << "TIMEOUT" << endl;
            return "";
        }
    }
    buffer[curs - buffer - 1] = '\0';
    //cout << "[" << buffer << "]" << endl;
    return buffer;
}

bool MatildaRobot::parse11(char wheel, const string& s, int& p, int& v, int& vv) {
    string needle = _logging ? "R110xx," : "R11xx,";
    needle[_logging ? 4 : 3] = wheel;
    needle[_logging ? 5 : 4] = wheel == 'L' ? '1' : '2';
//cout << "Looking for string [" << needle << "] in [" << s << "]" << endl;
    size_t p0 = s.find(needle), p1 = s.find("$");
//cout << s << " " << needle << " " << p0 << ";" << p1 << endl;
    if (p0 == string::npos || p1 == string::npos) return false;
    string ss = s.substr(p0, p1-p0);
    char* token = strtok(&ss[0], ",");
    token = strtok(NULL, ",");  if (token) p = atoi(token);
    token = strtok(NULL, ",");  if (token) v = atoi(token);
    token = strtok(NULL, ",");  if (token) vv = atoi(token);
//cout << "Parse of this [" << s << "], substr is [" << s.substr(p0, p1-p0) << "] resulted in " << p << " and " << v << endl;
    return true;
}

bool MatildaRobot::parse33(char wheel, const string& s, int& i, int& v, int& c, int& av) {
    string needle = "R33xx,";
    needle[3] = wheel;
    needle[4] = wheel == 'L' ? '1' : '2';
    //cout << "Looking for string [" << needle.str() << "]" << endl;
    size_t p0 = s.find(needle), p1 = s.find("$");
    if (p0 == string::npos || p1 == string::npos) return false;
    string ss = s.substr(p0, p1-p0);
    char* token = strtok(&ss[0], ",");
    token = strtok(NULL, ",");  if (token) i = atoi(token);
    token = strtok(NULL, ",");  if (token) v = atoi(token);
    token = strtok(NULL, ",");  if (token) c = atoi(token);
    token = strtok(NULL, ",");  if (token) av = atoi(token);
    //cout << "Parse of this [" << s << "], substr is [" << s.substr(p0, p1-p0) << "] resulted in " << av << " ( " << (double) (av - 44000) / (55000 - 44000) << " )" << endl;
    return true;
}

void MatildaRobot::updateOdometryTicks(char wheel, int v) {
    if (wheel == 'L') {
        _ticks_left = v;
        if (_odo_last_l == 0) _odo_last_l = v;
        else {
            _odo_val_l = v - _odo_last_l;
            _odo_last_l = v;
        }
    }
    else if (wheel == 'R') {
        _ticks_right = v;
        if (_odo_last_r == 0) _odo_last_r = v;
        else {
            _odo_val_r = v - _odo_last_r;
            _odo_last_r = v;
            updateOdometry(-_odo_val_l, _odo_val_r);
        }
    }
}

void MatildaRobot::updateOdometry(int left, int right) {
    double dr = right * _param_ticks_r;
    double dl = left * _param_ticks_l;
    double r = (dl + dr) / 2;
    double t = (dr - dl) / _param_baseline;
    _theta += t;
    _x += r * cos(_theta);
    _y += r * sin(_theta);
}


void MatildaRobot::velocity2ticks(int& right_ticks, int& left_ticks,
		    float translational_velocity, float rotatonal_velocity){
  // convert speed to distance sum/subtraction the _2 means the half of it
  float delta_plus_2  = translational_velocity*_param_time_interval;
  float delta_minus_2 = _param_baseline_half*rotatonal_velocity*_param_time_interval;
  right_ticks = (1./_param_ticks_r)* (delta_plus_2+delta_minus_2);
  left_ticks  = (1./_param_ticks_l)* (delta_plus_2-delta_minus_2);
}


void MatildaRobot::spinOnce() {
    int p, v, i, c, av, vv;
    int des_right=0,des_left=0; // in ticks per epoch
    if (_new_des_v) {   
        _new_des_v = false;
	velocity2ticks(des_right, des_left, _des_tv, _des_rv); 
	ostringstream cmdl, cmdr, cmdb;
        cmdl << "ll=" << des_left;
        cmdr << "rr=" << des_right;
        cmdb << "a=22";
        send(cmdl.str(), 1);
        send(cmdr.str(), 2);
        send(cmdb.str(), 0);

        cout << "Sending speed: [" << cmdl.str() << "], [" << cmdr.str() << "]" << endl;
    }
    if (_odom_extended_info_counter != _odom_extended_info_ratio) {
        send("b=11", 0);
        send(_logging ? "l=110" : "l=11", 1);
        if (parse11('L', recv(), p, v, vv)) {
            updateOdometryTicks('L', p);
            if (_logging) _logOfs << "L " << p << " " << (double) -v / 9900 << " " << vv << " " << des_left << endl;
        }
        send(_logging ? "r=110" : "r=11", 2);
        if (parse11('R', recv(), p, v, vv)) {
            updateOdometryTicks('R', p);
            if (_logging) _logOfs << "R " << p << " " << (double) v / 9900 << " " << vv << " " << des_right << endl;
        }
    }
    else {
        send("b=33", 0);
        send(_logging ? "l=110" : "l=11", 1);
        if (parse11('L', recv(), p, v, vv)) {
            updateOdometryTicks('L', p);
            if (_logging) _logOfs << "L " << p << " " << (double) -v / 9900 << " " << vv << " " << (_des_lin - _des_ang) << endl;
        }
        send(_logging ? "r=110" : "r=11", 2);
        if (parse11('R', recv(), p, v, vv)) {
            updateOdometryTicks('R', p);
            if (_logging) _logOfs << "R " << p << " " << (double) v / 9900 << " " << vv << " " << (_des_lin + _des_ang) << endl;
        }

        send("l=33", 1);
        if (parse33('L', recv(), i, v, c, av)) {
            _left_battery = (double) (av - 44000) / (55000 - 44000);
            if (_left_battery > 1.0) _left_battery = 1.0;
        }
        send("r=33", 2);
        if (parse33('R', recv(), i, v, c, av)) {
            _right_battery = (double) (av - 44000) / (55000 - 44000);
            if (_right_battery > 1.0) _right_battery = 1.0;
        }
        _odom_extended_info_counter = 0;
    }
    _odom_extended_info_counter++;
}

