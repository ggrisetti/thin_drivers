#pragma once
#include <string>
#include <fstream>

class MatildaRobot {
public:
  MatildaRobot();
  ~MatildaRobot();
  void connect(const std::string& device);
  void disconnect();
  void setSpeed(double tv, double rv);
  void getOdometry(double& x, double& y, double& theta);
  void getTicks(int& leftTicks, int &rightTicks) { leftTicks = _ticks_left; rightTicks = _ticks_right; }
  void getSpeed(double& tv, double& rv);
  void spinOnce();
  inline void setTimeInterval(double interval) {_param_time_interval=interval;}
  void setOdomExtendedInfoRatio(int ratio) { _odom_extended_info_ratio = ratio; }
  void getMotorBatteryStatus(double& left_battery, double& right_battery) { left_battery = _left_battery; right_battery = _right_battery; }
  void setLogFilename(const std::string& logfilename) { _logFilename = logfilename; _logging = logfilename != ""; if (_logging) _logOfs.open(logfilename.c_str()); }

protected:
  void send(const std::string& cmd, int channel);
  std::string recv();

  void velocity2ticks(int & right_ticks, int& left_ticks,
		      float translational_velocity, float rotatonal_velocity);

  bool parse11(char wheel, const std::string& s, int& p, int& v, int& vv);
  bool parse33(char wheel, const std::string& s, int& i, int& v, int&c, int& av);
  void updateOdometryTicks(char wheel, int v);
  void updateOdometry(int left, int right);

  int _fd;
  double _x, _y, _theta, _tv, _rv;
  double _des_tv, _des_rv;
  volatile bool _new_des_v;
  double _update_timestamp;

  double _left_battery, _right_battery;

  // parameters
  double _param_ticks_l, _param_ticks_r;
  double _param_baseline;
  double _param_baseline_half;
  double _param_time_interval;

  // odometry
  int _odo_last_l, _odo_last_r;
  int _odo_val_l, _odo_val_r;
  int _ticks_left, _ticks_right;

  // protocol used
  int _protocol;

  // counter and ratio between odometry and extended info
  int _odom_extended_info_ratio;
  int _odom_extended_info_counter;

  std::string _logFilename;
  std::ofstream _logOfs;
  bool _logging;

  int _des_lin;
  int _des_ang; 
};

