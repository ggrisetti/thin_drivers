#include <ros/ros.h>
#include <nav_msgs/Odometry.h>
#include <std_msgs/Float32.h>
#include <geometry_msgs/Twist.h>
#include <thin_matilda/Ticks.h>
#include "MatildaRobot.h"
#include "serial.h"
using namespace std;

volatile double tv = 0, rv = 0;
volatile bool new_velocity = false;
void commandVelCallback(const geometry_msgs::Twist::ConstPtr& twist) {
    tv = twist->linear.x;
    rv = twist->angular.z;
    new_velocity = true;
    //cout << "New velocity: " << tv << ", " << rv << endl;
}

int main(int argc, char** argv) {
    string serial_device;
    string odom_topic;
    string odom_frame_id;
    string command_vel_topic;
    string robot_type;
    string logfilename;
    double odom_speed_rate;
    double extended_info_rate;

    ros::init(argc, argv, "pioneer_node");
    ros::NodeHandle nh("~");
    nh.param("serial_device", serial_device, string("/dev/ttyElement"));
    nh.param("log", logfilename, string(""));
    nh.param("odom_topic", odom_topic, string("/odom"));
    nh.param("command_vel_topic", command_vel_topic, string("/cmd_vel"));
    nh.param("odom_frame_id", odom_frame_id, string("/odom"));
    nh.param("odom_speed_rate", odom_speed_rate, 30.0);
    nh.param("extended_info_rate", extended_info_rate, 1.0);

    ROS_INFO("Node thin_matilda_node is running with the following params: \n"
        "  serial_device: %s\n"
        "  odom_topic: %s\n"
        "  odom_frame_id: %s\n"
        "  command_vel_topic: %s\n"
        "  odom_speed_rate: %.2f Hz\n"
        "  extended_info_rate: %.2f Hz (real: %.2f Hz)",
        serial_device.c_str(), odom_topic.c_str(), odom_frame_id.c_str(), command_vel_topic.c_str(), odom_speed_rate, extended_info_rate, odom_speed_rate / (floor(odom_speed_rate / extended_info_rate)));
    if (logfilename != "") {
        ROS_INFO("Logging extended debug information to '%s'", logfilename.c_str());
    }
    ros::Subscriber command_vel_subscriber = nh.subscribe<geometry_msgs::Twist>(command_vel_topic, 10, &commandVelCallback);
    ros::Publisher odom_publisher = nh.advertise<nav_msgs::Odometry>(odom_topic, 1);
    ros::Publisher left_battery_publisher = nh.advertise<std_msgs::Float32>("/matilda/battery_left", 1);
    ros::Publisher right_battery_publisher = nh.advertise<std_msgs::Float32>("/matilda/battery_right", 1);
    ros::Publisher ticks_publisher = nh.advertise<thin_matilda::Ticks>("/matilda/ticks", 1);

    MatildaRobot robot;
    robot.connect(serial_device);
    robot.setOdomExtendedInfoRatio(odom_speed_rate / extended_info_rate);
    robot.setLogFilename(logfilename);
    int eir = odom_speed_rate / extended_info_rate;
    int eirc = 0;

    nav_msgs::Odometry odom;
    std_msgs::Float32 bat;
    thin_matilda::Ticks ticks;
    odom.header.frame_id = odom_frame_id;
    int seq = 0;

    ros::Rate rate(odom_speed_rate);

    while (ros::ok()) {
        ros::spinOnce();
        robot.spinOnce();

        // send speed to motors
        if (new_velocity) {
            new_velocity = false;
            robot.setSpeed(tv, rv);
        }

        // publish odometry
        double x, y, theta;
        robot.getOdometry(x, y, theta);
        odom.header.seq = seq;
        odom.header.stamp = ros::Time::now();
        odom.pose.pose.position.x = x;
        odom.pose.pose.position.y = y;
        odom.pose.pose.position.z = 0;
        double s = sin(theta/2);
        double c = cos(theta/2);
        odom.pose.pose.orientation.x = 0;
        odom.pose.pose.orientation.y = 0;
        odom.pose.pose.orientation.z = s;
        odom.pose.pose.orientation.w = c;
        odom_publisher.publish(odom);

        int lt, rt;
        robot.getTicks(lt, rt);
        ticks.leftTicks = lt;
        ticks.rightTicks = rt;
        ticks.header.seq = odom.header.seq;
        ticks.header.stamp = odom.header.stamp;
        ticks_publisher.publish(ticks);

        if (eirc == eir) {
            double bl, br;
            robot.getMotorBatteryStatus(bl, br);
            bat.data = bl;
            left_battery_publisher.publish(bat);
            bat.data = br;
            right_battery_publisher.publish(bat);
            eirc = 0;
        }

        rate.sleep();
        seq++;
        eirc++;
    } 
    robot.disconnect();
}
