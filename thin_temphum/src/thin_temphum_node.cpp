#include <string>
#include <ros/ros.h>
#include <ros/file_log.h>
#include <std_msgs/Float32.h>
#include <fstream>
#include "serial.h"
using namespace std;

class TempHumSensor {
public:
    TempHumSensor(ros::NodeHandle& nh) : nh(nh) {
        ros::NodeHandle nhp("~");
        nhp.param<string>("log", this->params.logFilename, "");
        ROS_INFO("Logging directory: %s", ros::file_log::getLogDirectory().c_str());
        if (this->params.logFilename != "") {
            char buf[1024];
            ROS_INFO("Logging temperature and humidity on %s (directory: %s).", this->params.logFilename.c_str(), getcwd(buf, 1024));
            temphumLog.open(this->params.logFilename.c_str());
        }
        string devicePort;
        nhp.param<string>("serial_device", devicePort, "/dev/ttyACM0");
        ROS_INFO("Opening port %s...", devicePort.c_str());
        serial_fd = serial_open(devicePort.c_str());
        serial_set_interface_attribs(serial_fd, B9600, 0);
        if (serial_fd <= 0) {
            ROS_ERROR("Cannot open port, aborting");
            exit(-1);
        }
        out.t0Pub = nh.advertise<std_msgs::Float32>("temphum/temperature0", 0.0);
        out.h0Pub = nh.advertise<std_msgs::Float32>("temphum/humidity0", 0.0);
        out.t1Pub = nh.advertise<std_msgs::Float32>("temphum/temperature1", 0.0);
        out.h1Pub = nh.advertise<std_msgs::Float32>("temphum/humidity1", 0.0);
        ROS_INFO("Reading...");
    }
    
    ~TempHumSensor() { }

    void spinOnce() {
        char buffer[128];
        int r = serial_readline(serial_fd, buffer, 128, 3.0);
        if (r <= 0) {
            ROS_ERROR("Error, timeout or serial port disconnected");
            exit(-1);
        }
        istringstream iss(string(buffer), ios_base::in);
        float t0, h0, t1, h1;
        iss >> t0 >> h0 >> t1 >> h1;
        std_msgs::Float32 obj;
        obj.data = t0;   out.t0Pub.publish(obj);
        obj.data = h0;   out.h0Pub.publish(obj);
        obj.data = t1;   out.t1Pub.publish(obj);
        obj.data = h1;   out.h1Pub.publish(obj);
        if (temphumLog.is_open()) {
            temphumLog << fixed << ros::Time::now().toSec() << " " << t0 << " " << h0 << " " << t1 << " " << h1 << endl;
        }
    }

protected:
    struct Out {
        ros::Publisher t0Pub;
        ros::Publisher h0Pub;
        ros::Publisher t1Pub;
        ros::Publisher h1Pub;
    } out;

    struct Params {
        std::string logFilename;
    } params;

    int serial_fd;
    ros::NodeHandle& nh;

    ofstream temphumLog;
};

int main(int argc, char** argv) {
    ros::init(argc, argv, "temphum");
    ros::NodeHandle nh;

    TempHumSensor ths(nh);

    ros::Rate rate(30.0);
    while (ros::ok()) {
        ros::spinOnce();
        ths.spinOnce();
        rate.sleep();
    }

    return 0;
}
