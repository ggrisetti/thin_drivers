#include "core/defs.h"
#include "core/euclidean_variable.h"
#include "core/multi_variable.h"
#include "core/se3_variable.h"
#include "core/camera_factor.h"
#include "kinematics/kinematics.hpp"
#include "core/se2_variable.h"
#include "core/laser_factor.h"


using namespace thin_calibrator;
using namespace std;



int main(int argc, char** argv) {

    float kl = 1;
    float kr = 1;
    float baseline = 0.5;

    Kinematics* robot = new DifferentialDrive();

    EuclideanVariable<3> odometry_paramiters;
    odometry_paramiters.setValue(Eigen::Vector3f(kl, kr, baseline));
    cerr << "odometry params: "<<odometry_paramiters.value().transpose() << endl;

    SE3Variable cstion_paramiters;
    Vector6f xtion_v;
    xtion_v << 0.02f, 0.f, 0.01f, 0.02f, 0.03f, 0.08f;
    cstion_paramiters.setValue(v2t(xtion_v));
    cerr << "cstion   params: "<<t2v(cstion_paramiters.value()).transpose() << endl;

    SE2Variable laser_parameters;
    laser_parameters.setValue(v2t(Eigen::Vector3f(0.03f, 0.01f, 0.1f)));
    cerr << "laser    params: "<<t2v(laser_parameters.value()).transpose() << endl;

    MultiVariable odom_xtion;
    odom_xtion.addVariable(&odometry_paramiters);
    odom_xtion.addVariable(&cstion_paramiters);
    cerr << "odom_xtion #parameterSet: " << odom_xtion.perturbationDimension() << endl;

    MultiVariable odom_laser;
    odom_laser.addVariable(&odometry_paramiters);
    odom_laser.addVariable(&laser_parameters);
    cerr << "odom_laser #parameterSet: " << odom_laser.perturbationDimension() << endl;

/// TEST CAMERA_FACTOR
    CameraFactor<3> camera_factor;
    camera_factor.setState(&odom_xtion);
    camera_factor.setRobot(robot);
    camera_factor.setMeasurement(v2t(xtion_v), Eigen::Vector2f(.1,.1));
    cerr << "camera_factor jacobian #rows " << camera_factor.jacobianRows() << " #cols " << camera_factor.jacobianCols() << endl;

    scalar_t error[6], perturbation[6];

    for(unsigned int i=0; i<6; ++i){
        perturbation[i] = 0.1;
    }

    camera_factor.computeError(error, perturbation);

    scalar_t jacobian_single_col[6*9] = {0.1};
    camera_factor.computeNumericJacobian(jacobian_single_col);

    for(unsigned int i=0; i<6*9; ++i){
        std::cerr<< jacobian_single_col[i] << " ";
    }

/// TEST LASER_FACTOR
    LaserFactor<3> laser_factor;
    laser_factor.setState(&odom_laser);
    laser_factor.setRobot(robot);
    laser_factor.setMeasurement(Eigen::Isometry2f::Identity(), Eigen::Vector2f(1,1));
    cerr << "laser_factor jacobian #rows " << laser_factor.jacobianRows() << " #cols " << laser_factor.jacobianCols() << endl;

    scalar_t l_error[3], l_perturbation[3];
    for(unsigned int i=0; i<3; ++i){
        l_perturbation[i] = 0.1;
    }

    laser_factor.computeError(l_error, l_perturbation);
    scalar_t l_jacobian_single_col[3*6] = {0.1};
    camera_factor.computeNumericJacobian(l_jacobian_single_col);

    for(unsigned int i=0; i<3*6; ++i){
        std::cerr<< l_jacobian_single_col[i] << " ";
    }

}
