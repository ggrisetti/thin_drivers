#ifndef _SE2_VARIABLE_H
#define _SE2_VARIABLE_H

#include "defs.h"
#include "variable.h"

namespace thin_calibrator{

class SE2Variable: public Variable<Isometry2_t> {
public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW

    SE2Variable(): Variable<Isometry2_t>(3){}

    virtual void oplus(const scalar_t* perturbation);
    virtual void resetValue() {_value.setIdentity();}
};

}

#endif
