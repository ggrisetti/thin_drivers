#ifndef _EUCLIDEAN_VARIABLE_H
#define _EUCLIDEAN_VARIABLE_H

#include "defs.h"
#include "variable.h"

namespace thin_calibrator{

template <int Dimension>
class EuclideanVariable: public Variable<Eigen::Matrix<scalar_t, Dimension, 1> > {
public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW
    EuclideanVariable(): Variable<Eigen::Matrix<scalar_t, Dimension, 1> >(Dimension){}
    virtual void oplus(const scalar_t* perturbation) {
        for (size_t i=0; i<Dimension; i++)
            this->_value[i]+=perturbation[i];
    }
    virtual void resetValue() { this->_value.setZero();}
};

}

#endif
