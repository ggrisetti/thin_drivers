#ifndef _BASE_FACTOR_H_
#define _BASE_FACTOR_H_

#include "defs.h"
#include "base_variable.h"

namespace thin_calibrator{

class BaseFactor {
public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW

    BaseFactor(int error_dimension) {
        _error_dimension=error_dimension;
        _state=0;
    }

    // to be redefined in the child classes.
    // Throws an exception if the state does not match the type for which the factor is designed
    virtual void setState(BaseVariable* state_){_state=state_;}

    virtual void computeError(scalar_t* error, const scalar_t* pertubation) = 0;

    inline int jacobianCols() const {
        if (!_state)
            throw std::runtime_error("BaseFactor: no state defined");
        return _state->perturbationDimension();
    }

    inline int jacobianRows() const {return _error_dimension;}

    virtual void computeNumericJacobian(scalar_t* jacobian_column_major,
                                        int row_stride=0,
                                        scalar_t epsilon=scalar_t(1e-3));

    BaseVariable* state() const {return _state;}
protected:
    int _error_dimension;
    BaseVariable* _state;
};


}

#endif
