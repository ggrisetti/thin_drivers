#ifndef _BASE_VARIABLE_
#define _BASE_VARIABLE_

#include "defs.h"
#include "solver.h"

namespace thin_calibrator{

class BaseVariable {
    friend class Solver;
public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW
    BaseVariable(int perturbation_dimension=0) {
        _perturbation_dimension=perturbation_dimension;
    }
    virtual ~BaseVariable() {}
    inline const int perturbationDimension() const {return _perturbation_dimension;}
    //! applies the oplus to the current value. tbo
    virtual void oplus(const scalar_t* perturbation)=0;

    virtual void push() = 0;
    virtual void pop() = 0;

    //! resets the value of the value to a standard value. tbo;
    virtual void resetValue()=0;

    //! assigns the indices in the jacobian based on the order
    virtual int updateHessianIndex(int base_index) {
        _hessian_index=base_index;
        return base_index+_perturbation_dimension;
    }

protected:
    mutable int _hessian_index;
    int _perturbation_dimension;
};

}

#endif
