#ifndef _MATH_UTILS_H_
#define _MATH_UTILS_H_

#include <Eigen/Dense>
#include <Eigen/Geometry>
#include <iostream>

namespace thin_calibrator{

typedef Eigen::Matrix< float, 6, 1 > Vector6f;
typedef Eigen::Matrix< double, 6, 1 > Vector6d;

typedef Eigen::Matrix< float, 5, 1 > Vector5f;
typedef Eigen::Matrix< double, 5, 1 > Vector5d;

//!converts from Vector6<Derived1> to Isometry3<Derived1>
//!@param v: a vector (vx, vy, vz, qx, qy, qz) representing the transform.
//!(qx, qy, qz) are the imaginary part of a normalized quaternion, with qw>0.
//!@returns the 3d Isometry corresponding to the transform described by v
//!
template <typename Derived1>
inline Eigen::Transform<Derived1,3,Eigen::Isometry> v2t(const Eigen::Matrix<Derived1, 6, 1>& v)
{
    Eigen::Transform<Derived1,3,Eigen::Isometry> T; //return an homogeneous transformation in a 3-dim space, aka 4x4
    T.template setIdentity();
    T.template translation() = v.template head<3>();
    Derived1 w = v.template block<3,1>(3,0).squaredNorm();
    if (w < (Derived1)1){
        w = sqrt(1 - w);
        Eigen::Quaternion<Derived1> q(w, v(3), v(4), v(5));
        T.template linear() = q.template toRotationMatrix();
    } else{
        Eigen::Matrix<Derived1, 3, 1> qv = v.template block<3,1>(3,0);
        qv.template normalize();
        Eigen::Quaternion<Derived1> q(0, qv(0), qv(1), qv(2));
        T.template linear() = q.template toRotationMatrix();
    }
    return T;
}

//!converts from Isometry3<Derived1 to Vector6<Derived1>
//!@param t: an isometry
//!@returns a vector (tx, ty, tz, qx, qy, qz) reptesenting the transform.
//!(qx, qy, qz) are the imaginary part of a normalized queternion, with qw>0.
template <typename Derived1>
inline Eigen::Matrix<Derived1, 6, 1> t2v(const Eigen::Transform<Derived1,3,Eigen::Isometry>& t){
    Eigen::Matrix<Derived1, 6, 1> v;
    v.template head<3>()= t.template translation();
    Eigen::Quaternion<Derived1> q(t.template linear());
    v(3) = q.template x();
    v(4) = q.template y();
    v(5) = q.template z();
    if (q.template w()<(Derived1)0)
        v.template block<3,1>(3,0) *= (Derived1)(-1);
    return v;
}


//!converts from Vector3<Derived1> to Isometry2<Derived1>
//!@param v: a vector (vx, vy, vtheta) representing the transform.
//!@returns the 2d Isometry corresponding to the transform described by v
//!
template<typename Derived1>
inline Eigen::Transform<Derived1, 2, Eigen::Isometry> v2t(const Eigen::Matrix<Derived1, 3, 1> &v){

    Eigen::Transform<Derived1, 2, Eigen::Isometry> A;
    Eigen::Rotation2D<Derived1> rot(v(2));
    Eigen::Matrix<Derived1,2,2> rot_e(rot);
    Eigen::Matrix<Derived1, 2, 1> trans(v(0), v(1));

    A.template linear() = rot_e;
    A.template translation() = trans;
    return A;
}

//!converts from Isometry2<Derived1> to Vector3<Derived1>
//!@param t: a 2d isometry
//!@returns a vector (tx, ty, ttheta) reptesenting the transform.
template<typename Derived1>
inline Eigen::Matrix<Derived1, 3, 1> t2v(const Eigen::Transform<Derived1, 2, Eigen::Isometry> &A){
    Eigen::Matrix<Derived1, 3, 1> t;
    t.template block(0,0,2,1) = A.template translation();
    t(2) = atan2(A(1,0),A(0,0));
    return t;
}

//!converts from Isometry3<Derived1> to Isometry2<Derived1>
//!@param t: a 3d isometry
//!@returns the 2d corresponding isometry (rotating only around z axis).
template<typename Derived1>
inline Eigen::Transform<Derived1, 2, Eigen::Isometry> iso2(const Eigen::Transform<Derived1, 3, Eigen::Isometry> &B){
    Eigen::Transform<Derived1, 2, Eigen::Isometry> A;
    A.template translation() = B.template translation().head(2);
    Eigen::Matrix<Derived1,3,1> v = B.template linear().eulerAngles(0,1,2);
    Eigen::Rotation2D<Derived1> rot(v(2));
    Eigen::Matrix<Derived1,2,2> rot_e(rot);
    A.template linear() = rot_e;

    return A;
}

//!normalize an angle between -pi/2 and pi/2
//!@param t: an angle
template <typename T>
inline void normalizeTheta(T &theta){
    theta = atan2(sin(theta),cos(theta));
}

//!normalize the elements of a Vector2<T> between -pi/2 and pi/2
//!@param t: a Vector2<T> to be normalized
template <typename T>
inline void normalizeTheta(Eigen::Matrix<T,2,1>& v){
    normalizeTheta(v(0));
    normalizeTheta(v(1));
}


}

#endif //UTILS_HPP
