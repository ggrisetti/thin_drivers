#include "se3_variable.h"

namespace thin_calibrator{

void SE3Variable::oplus(const scalar_t* perturbation){
    Isometry3_t dX;
    dX.translation()=Vector3_t(perturbation[0], perturbation[1], perturbation[2]);
    scalar_t squared_norm=
            perturbation[3]*perturbation[3]+
            +perturbation[4]*perturbation[4]+
            +perturbation[5]*perturbation[5];
    if (squared_norm>1) {
        throw std::runtime_error ("Degenerated unit quaternion");
    }
    scalar_t w=1-std::sqrt(squared_norm);
    dX.linear()=Quaternion_t(w, perturbation[3], perturbation[4], perturbation[5]).toRotationMatrix();
    _value=dX*_value;
}

}
