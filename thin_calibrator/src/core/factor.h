#ifndef _FACTOR_H_
#define _FACTOR_H_

#include "defs.h"
#include "base_factor.h"

namespace thin_calibrator{

template <typename VariableType_>
class Factor: public BaseFactor {
public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW
    typedef VariableType_ VariableType;
    Factor(int error_dimension): BaseFactor(error_dimension){
        _upcasted_state=0;
    }
    virtual void setState(BaseVariable* state_) {
        if (! state_){
            _state=0;
            _upcasted_state=0;
            return;
        }
        VariableType* var=dynamic_cast<VariableType*>(state_);
        if (! var){
            throw std::runtime_error("Factor: illegal type of variable set");
        }
        _state=var;
        _upcasted_state=var;
    }
protected:
    VariableType* _upcasted_state;
};

}

#endif
