#ifndef _LASER_FACTOR_H
#define _LASER_FACTOR_H

#include "defs.h"
#include "factor.h"
#include "multi_variable.h"
#include "se2_variable.h"
#include "euclidean_variable.h"
#include "../kinematics/kinematics.hpp"

namespace thin_calibrator{

template <int OdomDim>
class LaserFactor: public Factor<MultiVariable>{
public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW
    LaserFactor() : Factor(3){}

    void setState(BaseVariable* state_){
        Factor<MultiVariable>::setState(state_);
        if(! this->_state){
            _laser_offset = 0;
            _odom_parameters = 0;
            _robot = 0;
            return;
        }
        if(_upcasted_state->numVariables() != 2){
            throw std::runtime_error("num variables laser factor except");
        }
        _odom_parameters = dynamic_cast<EuclideanVariable<OdomDim>*>(_upcasted_state->variableAt(0));
        _laser_offset = dynamic_cast<SE2Variable*>(_upcasted_state->variableAt(1));
        if(!_odom_parameters)
            throw std::runtime_error("odom_params except in LaserFactor");
        if(!_laser_offset)
            throw std::runtime_error("laser_offset except in LaserFactor");
    }

    void computeError(scalar_t* error_, const scalar_t* perturbation){
        this->_state->push();
        this->_state->oplus(perturbation);

        scalar_t t_l = _ticks(0)*_odom_parameters->value()[0];
        scalar_t t_r = _ticks(1)*_odom_parameters->value()[1];

        Vector3_t pose(Vector3_t::Zero());
        Vector3_t odom_params(_odom_parameters->value()[0],
                              _odom_parameters->value()[1],
                              _odom_parameters->value()[2]);
        Vector2_t ticks(t_l, t_r);

        DifferentialDrive* dd_robot = dynamic_cast<DifferentialDrive*>(_robot);
        if(dd_robot){
            dd_robot->computeRobotStep(pose,
                                     ticks,
                                     odom_params);
        }else{
            throw std::runtime_error("til now only differential drive robots are allowed");
        }

        std::cerr<<"pose updated: " << pose.transpose() << std::endl;

        Isometry2_t robot_motion(Isometry2_t::Identity());
        robot_motion = v2t(pose);

        Isometry2_t prediction = _laser_offset->value().inverse() * robot_motion * _laser_offset->value();
        Isometry2_t error_t = _measurement.inverse() * prediction;

        Vector3_t err = t2v(error_t);

        for(unsigned int i=0; i< _error_dimension; ++i)
            error_[i] = err(i);

        this->_state->pop();
    }

    inline void setMeasurement(const Isometry2_t& measurement_, const Vector2_t& ticks_){
        _measurement = measurement_;
        _ticks = ticks_;
    }

    inline void setRobot(Kinematics* robot){
        _robot = robot;
    }

protected:
    Isometry2_t _measurement;
    Vector2_t _ticks;
    SE2Variable* _laser_offset;
    EuclideanVariable<OdomDim>* _odom_parameters;
    Kinematics* _robot;
};

}


#endif
