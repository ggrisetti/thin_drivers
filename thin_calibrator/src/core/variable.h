#ifndef _VARIABLE_H_
#define _VARIABLE_H_

#include "defs.h"
#include "base_variable.h"

namespace thin_calibrator{

template <typename ValueType_>
class Variable: public BaseVariable {
public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW
    typedef ValueType_ ValueType;
    Variable(int perturbation_dimension): BaseVariable(perturbation_dimension){}
    const ValueType& value() const { return _value;}
    void setValue(const ValueType& value_) {_value=value_;}
    virtual void push(){
        _stack.push(_value);
    }
    virtual void pop(){
        _value = _stack.top();
        _stack.pop();
    }

protected:
    ValueType _value;
    std::stack<ValueType> _stack;

};

}

#endif
