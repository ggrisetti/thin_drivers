#include "se2_variable.h"

namespace thin_calibrator{

void SE2Variable::oplus(const scalar_t* perturbation){
    Isometry2_t dX;
    dX = v2t(Vector3_t(perturbation[0], perturbation[1], perturbation[2]));
    _value=dX*_value;
}

}
