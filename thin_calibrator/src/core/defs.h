#ifndef _DEFS_H_
#define _DEFS_H_

#include <stdexcept>
#include <vector>
#include <stack>
#include <Eigen/Geometry>
#include <iostream>
#include "math_utils.h"

namespace thin_calibrator{
typedef float scalar_t;
typedef Eigen::Transform<scalar_t, 3, Eigen::Isometry> Isometry3_t;
typedef Eigen::Transform<scalar_t, 2, Eigen::Isometry> Isometry2_t;
typedef Eigen::Quaternion<scalar_t> Quaternion_t;
typedef Eigen::Matrix<scalar_t, 3, 1> Vector3_t;
typedef Eigen::Matrix<scalar_t, 2, 1> Vector2_t;
typedef Eigen::Matrix<scalar_t, 6, 1> Vector6_t;

}

#endif
