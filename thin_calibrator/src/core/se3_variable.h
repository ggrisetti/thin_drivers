#ifndef _SE3_VARIABLE_H
#define _SE3_VARIABLE_H

#include "defs.h"
#include "variable.h"

namespace thin_calibrator{

class SE3Variable: public Variable<Isometry3_t> {
public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW

    SE3Variable(): Variable<Isometry3_t>(6){}

    virtual void oplus(const scalar_t* perturbation);
    virtual void resetValue() {_value.setIdentity();}
};

}

#endif
