add_library(core_library
  defs.h
  math_utils.h
  base_factor.h        base_factor.cpp
  base_variable.h      base_variable.cpp
  camera_factor.h      camera_factor.cpp
  euclidean_variable.h euclidean_variable.cpp
  factor.h             factor.cpp
  laser_factor.h       laser_factor.cpp
  multi_variable.h     multi_variable.cpp
  se2_variable.h       se2_variable.cpp
  se3_variable.h       se3_variable.cpp
  solver.h             solver.cpp
  variable.h           variable.cpp
)

target_link_libraries(core_library
  thin_kinematics_library
)
