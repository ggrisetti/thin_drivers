#include "base_factor.h"


namespace thin_calibrator{

void BaseFactor::computeNumericJacobian(scalar_t* jacobian_column_major,
                                        int row_stride,
                                        scalar_t epsilon){
    if (! row_stride)
        row_stride=jacobianRows();

    //    const int matrix_dim=row_stride*jacobianCols();
    const int jacobian_cols=jacobianCols();
    scalar_t perturbation[jacobian_cols];

    for(int i=0; i<jacobian_cols; i++)
        perturbation[i]=scalar_t(0);

    scalar_t half_inverse_epsilon=0.5/epsilon;

    for(int c=0; c<jacobian_cols; c++) {
        scalar_t current_error_plus[row_stride];
        perturbation[c]=scalar_t(epsilon);
        computeError(current_error_plus, perturbation);

        scalar_t current_error_minus[row_stride];
        perturbation[c]=scalar_t(-epsilon);
        computeError(current_error_minus, perturbation);

        scalar_t* j_element=jacobian_column_major+row_stride*c;
        for (int r=0; r<_error_dimension; r++, j_element++){
            *j_element=half_inverse_epsilon*(current_error_plus[r]-current_error_minus[r]);
        }
    }
}


}
