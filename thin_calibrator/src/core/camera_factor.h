#ifndef _CAMERA_FACTOR_H
#define _CAMERA_FACTOR_H

#include "defs.h"
#include "factor.h"
#include "multi_variable.h"
#include "se3_variable.h"
#include "euclidean_variable.h"
#include "../kinematics/kinematics.hpp"

namespace thin_calibrator{

template <int OdomDim>
class CameraFactor: public Factor<MultiVariable>{
public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW
    CameraFactor() : Factor(6){}

    void setState(BaseVariable* state_){
        Factor<MultiVariable>::setState(state_);
        if(! this->_state){
            _camera_offset = 0;
            _odom_parameters = 0;
            _robot = 0;
            return;
        }
        if(_upcasted_state->numVariables() != 2){
            throw std::runtime_error("num variables camera factor except");
        }
        _odom_parameters = dynamic_cast<EuclideanVariable<OdomDim>*>(_upcasted_state->variableAt(0));
        _camera_offset = dynamic_cast<SE3Variable*>(_upcasted_state->variableAt(1));
        if(!_odom_parameters)
            throw std::runtime_error("odom_params except");
        if(!_camera_offset)
            throw std::runtime_error("camera_offset except");
    }

    void computeError(scalar_t* error_, const scalar_t* perturbation){
        this->_state->push();
        this->_state->oplus(perturbation);

        scalar_t t_l = _ticks(0)*_odom_parameters->value()[0];
        scalar_t t_r = _ticks(1)*_odom_parameters->value()[1];

        Vector3_t pose(Vector3_t::Zero());
        Vector3_t odom_params(_odom_parameters->value()[0],
                              _odom_parameters->value()[1],
                              _odom_parameters->value()[2]);
        Vector2_t ticks(t_l, t_r);

        DifferentialDrive* dd_robot = dynamic_cast<DifferentialDrive*>(_robot);
        if(dd_robot){
            dd_robot->computeRobotStep(pose,
                                     ticks,
                                     odom_params);
        }else{
            throw std::runtime_error("til now only differential drive robots are allowed");
        }

        std::cerr<<"pose updated: " << pose.transpose() << std::endl;

        Eigen::AngleAxis<scalar_t> rotation(pose(2), Vector3_t::UnitZ());
        Isometry3_t robot_motion(Isometry3_t::Identity());
        robot_motion.linear() = rotation.toRotationMatrix();
        robot_motion.translation() = Vector3_t(pose(0), pose(1), 0);

        Isometry3_t prediction = _camera_offset->value().inverse() * robot_motion * _camera_offset->value();
        Isometry3_t error_t = _measurement.inverse() * prediction;

        Vector6_t err = t2v(error_t);

        for(unsigned int i=0; i< _error_dimension; ++i)
            error_[i] = err(i);

        this->_state->pop();
    }

    inline void setMeasurement(const Isometry3_t& measurement_, const Vector2_t& ticks_){
        _measurement = measurement_;
        _ticks = ticks_;
    }

    inline void setRobot(Kinematics* robot){
        _robot = robot;
    }

protected:
    Isometry3_t _measurement;
    Vector2_t _ticks;
    SE3Variable* _camera_offset;
    EuclideanVariable<OdomDim>* _odom_parameters;
    Kinematics* _robot;
};

}


#endif
