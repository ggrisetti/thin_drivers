#ifndef KINEMATICS_HPP
#define KINEMATICS_HPP

#include <Eigen/Dense>
#include <Eigen/Geometry>
#include <iostream>
#include <stdint.h>
#include "../core/defs.h"
#include "../core/math_utils.h"

namespace thin_calibrator{

typedef scalar_t kin_type;
typedef Eigen::Matrix<kin_type, 3, 1> Vector3_k;
typedef Eigen::Matrix<kin_type, 2, 1> Vector2_k;
typedef Eigen::Matrix<kin_type, Eigen::Dynamic, 1> VectorX_k;


class Kinematics{
public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW
    virtual ~Kinematics(){}
    virtual void computeRobotStep(Vector3_k& pose,
                                  const VectorX_k& ticks,
                                  const VectorX_k& params) = 0;
};


class DifferentialDrive: public Kinematics{
public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW
    ~DifferentialDrive(){}
    void computeRobotStep(Vector3_k& pose,
                          const VectorX_k& ticks,
                          const VectorX_k& params);
};


class Omnidirectional: public Kinematics{
public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW
    ~Omnidirectional();
    virtual void computeOdomRobotStep(Vector3_k &pose,
                                      const VectorX_k &ticks,
                                      const VectorX_k &params) = 0;
};


class Youbot: public Omnidirectional{
public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW
    ~Youbot();
    void computeOdomRobotStep(Vector3_k &pose,
                              const VectorX_k &ticks,
                              const VectorX_k &params);
};


}



#endif //KINEMATICS_HPP
