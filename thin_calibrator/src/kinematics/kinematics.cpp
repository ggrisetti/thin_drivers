#include "kinematics.hpp"

namespace thin_calibrator{

const kin_type EPSILON=1e-6;

inline kin_type sinThetaOverTheta(kin_type theta) {
    if (fabs(theta)<EPSILON)
        return 1;
    return sin(theta)/theta;
}

inline kin_type oneMinisCosThetaOverTheta(kin_type theta) {
    if (fabs(theta)<EPSILON)
        return 0;
    return (1.0f-cos(theta))/theta;
}

#define _USE_TAYLOR_EXPANSION_

const kin_type cos_coeffs[]={0., 0.5 ,  0.    ,   -1.0/24.0,   0.    , 1.0/720.0};
const kin_type sin_coeffs[]={1., 0.  , -1./6. ,      0.    ,   1./120, 0.   };

void computeThetaTerms(kin_type& sin_theta_over_theta,
                       kin_type& one_minus_cos_theta_over_theta,
                       kin_type theta) {
#ifdef _USE_TAYLOR_EXPANSION_
    // evaluates the taylor expansion of sin(x)/x and (1-cos(x))/x,
    // where the linearization point is x=0, and the functions are evaluated
    // in x=theta
    sin_theta_over_theta=0;
    one_minus_cos_theta_over_theta=0;
    kin_type theta_acc=1;
    for (uint8_t i=0; i<6; i++) {
        if (i&0x1)
            one_minus_cos_theta_over_theta+=theta_acc*cos_coeffs[i];
        else
            sin_theta_over_theta+=theta_acc*sin_coeffs[i];
        theta_acc*=theta;
    }
#else
    sin_theta_over_theta=sinThetaOverTheta(theta);
    one_minus_cos_theta_over_theta=oneMinisCosThetaOverTheta(theta) ;
#endif
}



/*
* DIFFERENTIAL DRIVE KINEMATICS
*/
void DifferentialDrive::computeRobotStep(Vector3_k& pose,
                                         const VectorX_k &ticks,
                                         const VectorX_k &params){

    kin_type delta_l = ticks(0)*params(0);
    kin_type delta_r = ticks(1)*params(1);
    kin_type baseline = params(2);

    kin_type delta_plus=delta_r+delta_l;
    kin_type delta_minus=delta_r-delta_l;
    kin_type dth=delta_minus/baseline;
    kin_type one_minus_cos_theta_over_theta, sin_theta_over_theta;
    computeThetaTerms(sin_theta_over_theta, one_minus_cos_theta_over_theta, dth);
    kin_type dx=.5*delta_plus*sin_theta_over_theta;
    kin_type dy=.5*delta_plus*one_minus_cos_theta_over_theta;

    //apply the increment to the previous estimate
    kin_type s=sin(pose(2));
    kin_type c=cos(pose(2));
    pose(0) +=c*dx-s*dy;
    pose(1) +=s*dx+c*dy;
    pose(2) +=dth;
    normalizeTheta(pose(2));
}


/*
* YOUBOT KINEMATICS
*/
void Youbot::computeOdomRobotStep(Vector3_k &pose,
                                  const VectorX_k &ticks,
                                  const VectorX_k &params){

    kin_type geom_factor = params(3);
    kin_type w_FL = params(4);
    kin_type w_FR = params(5);
    kin_type w_BL = params(6);
    kin_type w_BR = params(7);

    // factory kinematics
    //    kin_type delta_longitudinal = (-ticks(0)*w_FL + ticks(1)*w_FR - ticks(2)*w_BL + ticks(3)*w_BR)/4;
    //    kin_type delta_trasversal = (ticks(0)*w_FL + ticks(1)*w_FR - ticks(2)*w_BL - ticks(3)*w_BR)/4;
    //    pose(2) += (ticks(0)*w_FL + ticks(1)*w_FR + ticks(2)*w_BL + ticks(3)*w_BR)/(4*geom_factor);

    // vrep compliant kinematics
    kin_type delta_longitudinal = (-ticks(0)*w_FL - ticks(1)*w_FR - ticks(2)*w_BL - ticks(3)*w_BR)/4;
    kin_type delta_trasversal = (ticks(0)*w_FL - ticks(1)*w_FR - ticks(2)*w_BL + ticks(3)*w_BR)/4;
    pose(2) += (ticks(0)*w_FL - ticks(1)*w_FR + ticks(2)*w_BL - ticks(3)*w_BR)/(4*geom_factor);

    normalizeTheta(pose(2));
    pose(0) = pose(0) + delta_longitudinal*cos(pose(2)) - delta_trasversal*sin(pose(2));
    pose(1) = pose(1) + delta_longitudinal*sin(pose(2)) + delta_trasversal*cos(pose(2));
}


}

